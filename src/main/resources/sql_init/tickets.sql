/*
Navicat MySQL Data Transfer

Source Server         : xampp_mysql
Source Server Version : 50536
Source Host           : localhost:3306
Source Database       : tickets

Target Server Type    : MYSQL
Target Server Version : 50536
File Encoding         : 65001

Date: 2019-12-04 14:17:12
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for booking
-- ----------------------------
DROP TABLE IF EXISTS `booking`;
CREATE TABLE `booking` (
  `booking_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '订票ID自增',
  `ticket_id` int(11) NOT NULL,
  `concert_id` int(11) NOT NULL COMMENT '演唱会id',
  `user_id` int(11) NOT NULL COMMENT '用户ID',
  `user_idcard` varchar(20) NOT NULL COMMENT '用户证件号码',
  `seat_type` varchar(10) NOT NULL COMMENT '坐席类别',
  `booking_price` decimal(10,4) NOT NULL COMMENT '订席票价',
  `booking_time` datetime NOT NULL COMMENT '订票时间',
  PRIMARY KEY (`booking_id`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf32;

-- ----------------------------
-- Records of booking
-- ----------------------------

-- ----------------------------
-- Table structure for concert
-- ----------------------------
DROP TABLE IF EXISTS `concert`;
CREATE TABLE `concert` (
  `concert_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '演出ID',
  `concert_name` varchar(100) NOT NULL COMMENT '演唱会名',
  `concert_place` varchar(100) NOT NULL COMMENT '演唱会地址',
  `concert_date` date NOT NULL COMMENT '演唱会日期',
  `concert_starttime` datetime NOT NULL COMMENT '开始时间',
  `singer` varchar(50) NOT NULL COMMENT '歌手或组合',
  PRIMARY KEY (`concert_id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf32;

-- ----------------------------
-- Records of concert
-- ----------------------------

-- ----------------------------
-- Table structure for concert_ticket
-- ----------------------------
DROP TABLE IF EXISTS `concert_ticket`;
CREATE TABLE `concert_ticket` (
  `ticket_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '席位ID',
  `concert_id` int(11) NOT NULL COMMENT '演唱会id',
  `seat_grade` varchar(10) NOT NULL COMMENT '席别',
  `seat_price` decimal(10,4) NOT NULL COMMENT '席别价格',
  `seat_total` int(4) NOT NULL COMMENT '席别座位总数量',
  `seat_left` int(4) NOT NULL COMMENT '席别座位剩余数量',
  PRIMARY KEY (`ticket_id`)
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=utf32;

-- ----------------------------
-- Records of concert_ticket
-- ----------------------------

-- ----------------------------
-- Table structure for repair_feedback
-- ----------------------------
DROP TABLE IF EXISTS `repair_feedback`;
CREATE TABLE `repair_feedback` (
  `repair_feedback_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '报修反馈Id',
  `repair_id` int(11) NOT NULL COMMENT '报修Id',
  `supplier_id` varchar(50) DEFAULT '' COMMENT '供应商用户',
  `feedback_date` datetime DEFAULT NULL COMMENT '反馈日期',
  `feedback_answer` varchar(255) DEFAULT '' COMMENT '反馈回答',
  `supplier_express_no` varchar(20) DEFAULT NULL COMMENT '供应商快递单号',
  `add_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`repair_feedback_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of repair_feedback
-- ----------------------------

-- ----------------------------
-- Table structure for repair_order
-- ----------------------------
DROP TABLE IF EXISTS `repair_order`;
CREATE TABLE `repair_order` (
  `repair_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '报修Id',
  `customer_id` int(11) NOT NULL COMMENT '客户',
  `device_no` varchar(20) DEFAULT '' COMMENT '设备编号',
  `problem` varchar(255) DEFAULT '' COMMENT '问题描述',
  `repair_date` datetime DEFAULT NULL COMMENT '报修日期',
  `customer_express_no` varchar(20) DEFAULT '' COMMENT '客户快递单号',
  `repair_status` int(11) DEFAULT NULL COMMENT '报修状态',
  `add_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`repair_id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of repair_order
-- ----------------------------

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '用户ID',
  `user_loginname` varchar(50) NOT NULL COMMENT '用户登录名，管理员为admin，普通用户为身份证号',
  `user_password` varchar(50) NOT NULL COMMENT '登录密码',
  `user_type` int(2) NOT NULL COMMENT '记录用户类型（0管理员，1普通用户）',
  `user_status` int(2) DEFAULT '0' COMMENT '用户状态（0正常，1禁用，2删除）',
  `last_login_time` datetime DEFAULT NULL COMMENT '最近登录时间',
  `register_time` datetime DEFAULT NULL COMMENT '注册时间',
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf32;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('1', 'admin', 'admin', '0', '0', '2019-12-04 10:58:52', null);

-- ----------------------------
-- Table structure for user_detail
-- ----------------------------
DROP TABLE IF EXISTS `user_detail`;
CREATE TABLE `user_detail` (
  `detail_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '用户详情ID',
  `user_id` int(11) NOT NULL COMMENT '用户ID',
  `user_loginname` varchar(50) NOT NULL COMMENT '用户登录名',
  `user_name` varchar(50) DEFAULT NULL COMMENT '用户姓名',
  `user_gender` varchar(4) DEFAULT NULL COMMENT '性别',
  `user_idcard` varchar(20) DEFAULT NULL COMMENT '用户证件号码',
  `user_phone` varchar(20) DEFAULT NULL COMMENT '用户手机号码',
  `user_email` varchar(50) DEFAULT NULL COMMENT '用户邮箱',
  `user_liveplace` varchar(200) DEFAULT NULL COMMENT '用户住址',
  PRIMARY KEY (`detail_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf32;

-- ----------------------------
-- Records of user_detail
-- ----------------------------
INSERT INTO `user_detail` VALUES ('1', '1', 'admin', '管理员', null, null, null, null, null);
