(function ($) {
    'use strict';
    var idCard;
    var userId;
    $(document).ready(function () {
        $("#header").load('../public/header.html');

        var loginName = getCookie("loginName");
        var userName = getCookie("userName");
        var userType = getCookie("userType");
        userId = getCookie("userId");

        if (userType == 0) {
            $("#sidebar").load('../public/adminsidebar.html');
        } else if(userType == 1) {
            $("#sidebar").load('../public/sidebar.html');
        } else if(userType == 2) {
            $("#sidebar").load('../public/customerSidebar.html');
            window.location.href = "./myRepairOrder.html";
        } else if(userType == 3) {
            $("#sidebar").load('../public/supplierSidebar.html');
            window.location.href = "./repairFeedback.html";
        }

        $.ajax({
            type: "GET",
            url: "/api/getUserInfo",    //向后端请求数据的url
            data: {
                loginName: loginName
            },
            success: function (data) {
                userId = data.user ? data.user.userId : '';
                idCard = data.userIdCard | '';
            }
        });

        initData();

        $(".searchBtn").click(function () {

            var keywords = $("#searchtext").val();
            $.ajax({
                type: "GET",
                url: "/api/keywordsConcert",    //向后端请求数据的url
                data: {
                    keywords: keywords
                },
                success: function (data) {
                    if (data.length > 0) {
                        showForm(data);
                    }
                }
            });
        });
    });

    //获取浏览器中的Cookie
    function getCookie(cName) {
        var cookieString = decodeURI(document.cookie);
        var cookieArray = cookieString.split("; ");
        // console.log(cookieArray.length);
        for (var i = 0; i < cookieArray.length; i++) {
            var cookieNum = cookieArray[i].split("=");
            // console.log(cookieNum.toString());
            var cookieName = cookieNum[0];
            var cookieValue = cookieNum[1];

            if (cookieName == cName) {
                return cookieValue;
            }
        }
        return false;
    }

    //随机数
    function diu_Randomize(b, e) {
        if (!b && b != 0 || !e) {
            return "?";
        }
        return Math.floor(( Math.random() * e ) + b);
    }

    function loadColor() {
        var keyArray = new Array();
        keyArray.push("blue");
        keyArray.push("purple");
        keyArray.push("red");
        keyArray.push("green");
        return keyArray[diu_Randomize(0, 3)];
    }

    function initData() {
        $.ajax({
            type: "GET",
            url: "/api/allConcert",    //向后端请求数据的url
            data: {},
            success: function (data) {
                if (data.length > 0) {
                    showForm(data);
                }
            }
        });
    }

    function showForm(data) {
        $("#panelrow").empty();
        for (var i = 0; i < data.length; i++) {
            var color = loadColor();
            var dom = '<div class="am-u-lg-3 am-u-md-6 am-u-sm-12 " style="float: left">\n' +
                '<div class="dashboard-stat ' + color + '">\n' +
                '<div class="visual">\n' +
                '<i class="am-icon-comments-o"></i>\n' +
                '</div>\n' +
                '<div class="details">\n' +
                '<div class="number newstyle">' + data[i].concertName + '</div>\n' +
                '<div class="desc">' + (new Date(data[i].concertDate)).toLocaleDateString() + '</div>\n' +
                '</div>\n' +
                '<div class="more_' + data[i].concertId + '">' +
                '<a class="more"> 点击购票\n' +
                '<i class="m-icon-swapright m-icon-white"></i>\n' +
                '</a>\n' +
                '</div>\n' +
                '</div>';
            $("#panelrow").append(dom);
        }

        $(".more").click(function () {
            var classArray = $(this).parent().attr('class').split('_');
            var concertId = classArray[1];
            $.ajax({
                type: "GET",
                url: "/api/idConcert",    //向后端请求数据的url
                data: {
                    concertId: concertId
                },
                success: function (data) {
                    if (data) {
                        $(".concert_name").text(data.concertName);
                        $("#time").text((new Date(data.concertDate)).toLocaleDateString());
                        $("#startTime").text((new Date(data.concertStartTime)).toLocaleString());
                        $("#address").text(data.concertPlace);
                    }
                }
            });
            $.ajax({
                type: "GET",
                url: "/api/idConcertTicket",    //向后端请求数据的url
                data: {
                    concertId: concertId
                },
                success: function (data) {
                    // console.log(data);
                    if (data) {
                        for (var i = 0; i < data.length; i++) {
                            $("#seatType" + (i + 1)).text("席别" + data[i].seatGrade + "(" + data[i].seatPrice + "元) 剩余:" + data[i].seatLeft);
                            if (data[i].seatLeft == 0) {
                                $("#radio_" + (i + 1 )).attr("disabled", true);
                            }
                        }
                    } else {
                        $('#booking-success-modal').find(".am-modal-hd").text("查询错误");
                        $('#booking-success-modal').modal('open');
                    }
                }
            });
            $('#booking-concert').modal({
                relatedTarget: this,
                onConfirm: function (options) {
                    booking(concertId);
                },
                onCancel: function () {
                    return;
                }
            });
        });
    }

    function booking(concertId) {
        var seatType = $("input[name='docInlineRadio']:checked").val();
        $.ajax({
            type: "post",
            url: "/api/addBooking",    //向后端请求数据的url
            data: {
                concertId: concertId,
                userId: userId,
                userIdCard: idCard,
                seatType: seatType
            },
            success: function (data) {
                // console.log(data);
                if (!data.status) {
                    $('#booking-concert').modal('close');
                    $('#booking-success-modal').modal('open');
                }
            },
            error: function (data) {
                $('#booking-success-modal').find(".am-modal-hd").text(data.responseJSON.message);
                $('#booking-success-modal').modal('open');
            }
        });
    }
})(jQuery);
