(function ($) {
    'use strict';

    $(document).ready(function () {
        $("#header").load('../public/header.html');

        var loginName = getCookie("loginName");
        var userName = getCookie("userName");
        var userType = getCookie("userType");
        var userId = getCookie("userId");

        if (userType == 0) {
            $("#sidebar").load('../public/adminsidebar.html');
        } else if(userType == 1) {
            $("#sidebar").load('../public/sidebar.html');
        } else if(userType == 2) {
            $("#sidebar").load('../public/customerSidebar.html');
        } else if(userType == 3) {
            $("#sidebar").load('../public/supplierSidebar.html');
        }

        $(".searchBtn").click(function () {
            var deviceNO = $("#searchText").val();
            $.ajax({
                type: "GET",
                url: "/api/findRepairOrder",    //向后端请求数据的url
                data: {
                    customerId: userId
                    ,deviceNO: deviceNO
                    //,startDate: startDate
                    //,endDate: endDate
                    //,expressNO: expressNO
                    //,status: status
                },
                success: function (data) {
                    console.log(data);
                    if (data.data.length > 0) {
                        showForm(data.data);
                    }
                }
            });
        }).trigger("click");

        function deleteRepairOrder(repairId, customerId) {
            $.ajax({
                type: "POST",
                url: "/api/cancelRepairOrder",    //向后端请求数据的url
                data: {
                    repairId: repairId,
                    customerId: customerId
                },
                success: function (data) {
                    // console.log(data);
                    if (!data.status) {
                        $('#delete-success-modal').modal('open');
                        // initForm(userId);
                    }
                }
            });
        }

        function showForm(data) {
            $("#formbody").empty();
            for (var i = 0; i < data.length; i++) {
                var dom = ' <tr>\n' +
                    '<td style="display: none" class="repair_id">\n' +
                    '<input type="hidden" id="repairId" value="' + data[i].repairId + '"/>\n'+
                    '<input type="hidden" id="customerId" value="' + data[i].customerId + '"/>\n' +
                    '</td>\n' +
                    '<td>' + data[i].deviceNO + '</td>\n' +
                    '<td class="am-hide-sm-only">' + (new Date(data[i].repairDate)).toLocaleDateString() + '</td>\n' +
                    '<td>' + (data[i].repairStatus == 0 ? "已报修" : data[i].repairStatus == 1 ? "已反馈" : "已删除") + '</td>\n' +
                    '<td>\n' +
                    '<div class="am-btn-toolbar">\n' +
                    '<div class="am-btn-group am-btn-group-xs">\n' +
                    '<span class="am-btn am-btn-default am-btn-xs am-text-secondary detailBtn"><span class="am-icon-pencil-square-o"></span> 详情</span>\n';
                    // if(data[i].repairStatus == 0) {
                    //     dom +='<span class="am-btn am-btn-default am-btn-xs am-text-danger am-hide-sm-only deleteBtn"><span class="am-icon-trash-o"></span> 删除</span>\n';
                    // }
                    dom +='</div>\n' +
                    '</div>\n' +
                    '</td>\n' +
                    '</tr>';
                $("#formbody").append(dom);
            }

            $(".detailBtn").click(function () {
                var td_repair = $(this).parent().parent().parent().prevAll().filter(".repair_id");
                var repairId = td_repair.children("#repairId").val();
                $.ajax({
                    type: "GET",
                    url: "/api/getRepairOrderDetail",    //向后端请求数据的url
                    data: {
                        repairId: repairId
                    },
                    success: function (data) {
                        // console.log(data);
                        $("#deviceNO").val(data.data.deviceNO);
                        $("#problem").val(data.data.problem);
                        $("#repairDate").val((new Date(data.data.repairDate)).toLocaleDateString());
                        $("#repairStatus").val(data.data.repairStatus == 0 ? "已报修" : "已反馈");
                        $("#customerExpressNO").val(data.data.customerExpressNO);
                        if(data.data.feedbackDate != null) {
                            $("#feedbackDate").val((new Date(data.data.feedbackDate)).toLocaleDateString());
                        }
                        $("#feedbackAnswer").val(data.data.feedbackAnswer);
                        $("#supplierExpressNO").val(data.data.supplierExpressNO);
                        $('#update-repair').modal({
                            relatedTarget: this,
                            onConfirm: function (options) {
                                return;
                            },
                            // closeOnConfirm: false,
                            onCancel: function () {
                                return;
                            }
                        });
                    }
                });
            });

            $(".deleteBtn").click(function () {
                var td_repair = $(this).parent().parent().parent().prevAll().filter(".concert_id");
                var repairId = td_repair.children("#repairId").val();
                var customerId = td_repair.children("#customerId").val();

                $('#delete-confirm').modal({
                    relatedTarget: this,
                    onConfirm: function (options) {
                        deleteRepairOrder(repairId, customerId);
                    },
                    // closeOnConfirm: false,
                    onCancel: function () {
                        return;
                    }
                });
            });
        }
    });

    //获取浏览器中的Cookie
    function getCookie(cName) {
        var cookieString = decodeURI(document.cookie);
        var cookieArray = cookieString.split("; ");
        // console.log(cookieArray.length);
        for (var i = 0; i < cookieArray.length; i++) {
            var cookieNum = cookieArray[i].split("=");
            // console.log(cookieNum.toString());
            var cookieName = cookieNum[0];
            var cookieValue = cookieNum[1];

            if (cookieName == cName) {
                return cookieValue;
            }
        }
        return false;
    }
})(jQuery);
