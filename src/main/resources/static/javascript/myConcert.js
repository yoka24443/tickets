(function ($) {
    'use strict';

    var idCard;
    var userId;

    $(document).ready(function () {
        $("#header").load('../public/header.html');

        var loginName = getCookie("loginName");
        var userName = getCookie("userName");
        var userType = getCookie("userType");
        userId = getCookie("userId");

        if (userType == 0) {
            $("#sidebar").load('../public/adminsidebar.html');
        } else if(userType == 1) {
            $("#sidebar").load('../public/sidebar.html');
        } else if(userType == 2) {
            $("#sidebar").load('../public/customerSidebar.html');
        } else if(userType == 3) {
            $("#sidebar").load('../public/supplierSidebar.html');
        }

        $.ajax({
            type: "GET",
            url: "/api/getUserInfo",    //向后端请求数据的url
            data: {
                loginName: loginName
            },
            success: function (data) {
                idCard = data.userIdCard;
                userId = data.user.userId;
                initForm(userId);
            }
        });

        $(".searchBtn").click(function () {
            var keywords = $("#searchtext").val();
            $.ajax({
                type: "GET",
                url: "/api/keywordsConcert",    //向后端请求数据的url
                data: {
                    keywords: keywords
                },
                success: function (data) {
                    if (data.length > 0) {
                        showForm(data);
                    }
                }
            });
        });

        function initForm(userId) {
            $.ajax({
                type: "get",
                url: "/api/userIdBooking",    //向后端请求数据的url
                data: {
                    userId: userId
                },
                success: function (data) {
                    // console.log(data)
                    if (!data.status) {
                        showForm(data);
                    }
                }
            });
        }

        function deleteBooking(bookingId, concertId, userId) {
            $.ajax({
                type: "POST",
                url: "/api/refundBooking",    //向后端请求数据的url
                data: {
                    bookingId: bookingId,
                    concertId: concertId,
                    userId: userId
                },
                success: function (data) {
                    // console.log(data);
                    if (!data.status) {
                        $('#delete-success-modal').modal('open');
                        initForm(userId);
                    }
                }
            });
        }

        function showForm(data) {
            $("#formbody").empty();
            for (var i = 0; i < data.length; i++) {
                var dom = ' <tr>\n' +
                    '<td style="display: none" class="concert_id">\n' +
                    '<input type="hidden" id="concertId" value="' + data[i].concert.concertId + '"/>\n'+
                    '<input type="hidden" id="bookingId" value="' + data[i].bookingId + '"/>\n' +
                    '<input type="hidden" id="userId" value="' + data[i].userId + '"/>\n' +
                    '</td>\n' +
                    '<td>' + data[i].concert.concertName + '</td>\n' +
                    '<td>' + data[i].concert.singer + '</td>\n' +
                    '<td class="am-hide-sm-only">' + (new Date(data[i].concert.concertDate)).toLocaleDateString() + '</td>\n' +
                    '<td class="am-hide-sm-only">' + data[i].concert.concertPlace + '</td>\n' +
                    '<td>\n' +
                    '<div class="am-btn-toolbar">\n' +
                    '<div class="am-btn-group am-btn-group-xs">\n' +
                    '<span class="am-btn am-btn-default am-btn-xs am-text-secondary detailBtn"><span class="am-icon-pencil-square-o"></span> 详情</span>\n' +
                    '<span class="am-btn am-btn-default am-btn-xs am-text-danger am-hide-sm-only deleteBtn"><span class="am-icon-trash-o"></span> 删除</span>\n' +
                    '</div>\n' +
                    '</div>\n' +
                    '</td>\n' +
                    '</tr>';
                $("#formbody").append(dom);
            }

            $(".detailBtn").click(function () {
                var td_concert = $(this).parent().parent().parent().prevAll().filter(".concert_id");
                var concertId = td_concert.children("#concertId").val();
                $.ajax({
                    type: "GET",
                    url: "/api/idConcert",    //向后端请求数据的url
                    data: {
                        concertId: concertId
                    },
                    success: function (data) {
                        console.log(data);
                        $("#concertName").val(data.concertName);
                        $("#singer").val(data.singer);
                        $("#date").val((new Date(data.concertDate)).toLocaleDateString());
                        $("#startTime").val((new Date(data.concertStartTime)).getTime());
                        $("#address").val(data.concertPlace);
                        $('#update-concert').modal({
                            relatedTarget: this,
                            onConfirm: function (options) {
                                return;
                            },
                            // closeOnConfirm: false,
                            onCancel: function () {
                                return;
                            }
                        });
                    }
                });
            });

            $(".deleteBtn").click(function () {
                var td_concert = $(this).parent().parent().parent().prevAll().filter(".concert_id");
                var concertId = td_concert.children("#concertId").val();
                var bookingId = td_concert.children("#bookingId").val();
                var userId = td_concert.children("#userId").val();
                $('#delete-confirm').modal({
                    relatedTarget: this,
                    onConfirm: function (options) {
                        deleteBooking(bookingId, concertId.toString(), userId);
                    },
                    // closeOnConfirm: false,
                    onCancel: function () {
                        return;
                    }
                });
            });
        }
    });

    //获取浏览器中的Cookie
    function getCookie(cName) {
        var cookieString = decodeURI(document.cookie);
        var cookieArray = cookieString.split("; ");
        // console.log(cookieArray.length);
        for (var i = 0; i < cookieArray.length; i++) {
            var cookieNum = cookieArray[i].split("=");
            // console.log(cookieNum.toString());
            var cookieName = cookieNum[0];
            var cookieValue = cookieNum[1];

            if (cookieName == cName) {
                return cookieValue;
            }
        }
        return false;
    }
})(jQuery);
