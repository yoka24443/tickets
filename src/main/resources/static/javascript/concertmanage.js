(function ($) {
    'use strict';
    $(document).ready(function () {

        $("#header").load('../public/header.html');

        var loginName = getCookie("loginName");
        var userName = getCookie("userName");
        var userType = getCookie("userType");
            var userId = getCookie("userId");

        if (userType == 0) {
            $("#sidebar").load('../public/adminsidebar.html');
        } else if(userType == 1) {
            $("#sidebar").load('../public/sidebar.html');
        } else if(userType == 2) {
            $("#sidebar").load('../public/customerSidebar.html');
        } else if(userType == 3) {
            $("#sidebar").load('../public/supplierSidebar.html');
        }

        initForm();

        $(".searchBtn").click(function () {
            var keywords = $("#searchtext").val();
            $.ajax({
                type: "GET",
                url: "/api/keywordsConcert",    //向后端请求数据的url
                data: {
                    keywords: keywords
                },
                success: function (data) {
                    if (data.length > 0) {
                        showForm(data);
                    }
                }
            });
        });

        function initForm() {
            $.ajax({
                type: "GET",
                url: "/api/allConcert",    //向后端请求数据的url
                data: {},
                success: function (data) {
                    if (data.length > 0) {
                        showForm(data);
                    }
                }
            });
        }

        function deleteConcert(concertId) {
            console.log(concertId)
            $.ajax({
                type: "post",
                url: "/api/deleteConcert",    //向后端请求数据的url
                data: {
                    concertId: concertId
                },
                success: function (data) {
                    // console.log(data);
                    if (data == "0") {
                        $('#delete-success-modal').modal('open');
                        initForm();
                    }
                }
            });
        }

        function showForm(data) {
            $("#formbody").empty();
            for (var i = 0; i < data.length; i++) {
                var dom = ' <tr>\n' +
                    '<td style="display: none" class="concert_id">' + data[i].concertId + '</td>\n' +
                    '<td>' + data[i].concertName + '</td>\n' +
                    '<td>' + data[i].singer + '</td>\n' +
                    '<td class="am-hide-sm-only">' + (new Date(data[i].concertDate)).toLocaleDateString() + '</td>\n' +
                    '<td class="am-hide-sm-only">' + data[i].concertPlace + '</td>\n' +
                    '<td>\n' +
                    '<div class="am-btn-toolbar">\n' +
                    '<div class="am-btn-group am-btn-group-xs">\n' +
                    '<span class="am-btn am-btn-default am-btn-xs am-text-secondary editBtn"><span class="am-icon-pencil-square-o"></span> 编辑</span>\n' +
                    '<span class="am-btn am-btn-default am-btn-xs am-text-danger am-hide-sm-only deleteBtn"><span class="am-icon-trash-o"></span> 删除</span>\n' +
                    '</div>\n' +
                    '</div>\n' +
                    '</td>\n' +
                    '</tr>';
                $("#formbody").append(dom);
            }
            $(".deleteBtn").click(function () {
                var concertId = $(this).parent().parent().parent().prevAll().filter(".concert_id").text();
                $('#delete-confirm').modal({
                    relatedTarget: this,
                    onConfirm: function (options) {
                        deleteConcert(concertId.toString());
                    },
                    // closeOnConfirm: false,
                    onCancel: function () {
                        return;
                    }
                });
            });

            $(".editBtn").click(function () {
                var concertId = $(this).parent().parent().parent().prevAll().filter(".concert_id").text();
                $.ajax({
                    type: "GET",
                    url: "/api/idConcert",    //向后端请求数据的url
                    data: {
                        concertId: concertId
                    },
                    success: function (data) {
                        // console.log(data);
                        $("#concertname").val(data.concertName);
                        $("#singer").val(data.singer);
                        $("#date").val((new Date(data.concertDate)).toLocaleDateString());
                        $("#starttime").val((new Date(data.concertStartTime)).toLocaleString());
                        $("#address").val(data.concertPlace)
                        $('#update-concert').modal({
                            relatedTarget: this,
                            onConfirm: function (options) {
                                var concertName_ = $("#concertname").val();
                                var singer_ = $("#singer").val();
                                var date_ = $("#date").val();
                                var startTime_ = $("#starttime").val();
                                var address_ = $("#address").val();
                                $.ajax({
                                    type: "PUT",
                                    url: "/api/modifyDateConcert",    //向后端请求数据的url
                                    data: {
                                        concertId: concertId,
                                        concertName: concertName_,
                                        singer: singer_,
                                        concertDate: date_,
                                        concertStartTime: startTime_,
                                        concertPlace: address_
                                    },
                                    success: function (data) {
                                        if (data) {
                                            $('#update-success').modal('open');
                                            initForm();
                                        } else {
                                            $('#update-error').modal('open');
                                        }
                                    }
                                });
                            },
                            // closeOnConfirm: false,
                            onCancel: function () {
                                return;
                            }
                        });
                    }
                });
            });
        }
    });

    //获取浏览器中的Cookie
    function getCookie(cName) {
        var cookieString = decodeURI(document.cookie);
        var cookieArray = cookieString.split("; ");
        // console.log(cookieArray.length);
        for (var i = 0; i < cookieArray.length; i++) {
            var cookieNum = cookieArray[i].split("=");
            // console.log(cookieNum.toString());
            var cookieName = cookieNum[0];
            var cookieValue = cookieNum[1];

            if (cookieName == cName) {
                return cookieValue;
            }
        }
        return false;
    }
})(jQuery);
