(function ($) {
    'use strict';
    $(document).ready(function () {

        $("#header").load('../public/header.html');

        var loginName = getCookie("loginName");
        var userName = getCookie("userName");
        var userType = getCookie("userType");
        var userId = getCookie("userId");

        if (userType == 0) {
            $("#sidebar").load('../public/adminsidebar.html');
        } else if(userType == 1) {
            $("#sidebar").load('../public/sidebar.html');
        } else if(userType == 2) {
            $("#sidebar").load('../public/customerSidebar.html');
        } else if(userType == 3) {
            $("#sidebar").load('../public/supplierSidebar.html');
        }

        $("#subBtn").click(function () {
            var concertName = $("#concertName").val();
            var singer = $("#singer").val();
            var date = $("#date").val();
            var startTime = $("#startTime").val();
            var address = $("#address").val();
            var number_1 = $("#number_1").val();
            var price_1 = $("#price_1").val();
            var number_2 = $("#number_2").val();
            var price_2 = $("#price_2").val();
            var number_3 = $("#number_3").val();
            var price_3 = $("#price_3").val();

            $.ajax({
                type: "POST",
                url: "/api/addConcert",    //向后端请求数据的url
                data: {
                    concertName: concertName,
                    singer: singer,
                    concertDate: date,
                    concertStartTime: startTime,
                    concertPlace: address,
                    grade_1: 'A',
                    price_1: price_1,
                    seat_1: number_1,
                    grade_2: 'B',
                    price_2: price_2,
                    seat_2: number_2,
                    grade_3: 'C',
                    price_3: price_3,
                    seat_3: number_3
                },
                success: function (data) {
                    console.log(data);
                    if (data) {
                        $('#success-modal').modal('open');
                        $("#concertName").val("");
                        $("#singer").val("");
                        $("#date").val("");
                        $("#startTime").val("");
                        $("#address").val("");
                        $("#number_1").val("");
                        $("#price_1").val("");
                        $("#number_2").val("");
                        $("#price_2").val("");
                        $("#number_3").val("");
                        $("#price_3").val("");
                    } else {
                        $('#error-modal').modal('open');
                    }

                }
            });
        });
    });

    //获取浏览器中的Cookie
    function getCookie(cName) {
        var cookieString = decodeURI(document.cookie);
        var cookieArray = cookieString.split("; ");
        // console.log(cookieArray.length);
        for (var i = 0; i < cookieArray.length; i++) {
            var cookieNum = cookieArray[i].split("=");
            // console.log(cookieNum.toString());
            var cookieName = cookieNum[0];
            var cookieValue = cookieNum[1];

            if (cookieName == cName) {
                return cookieValue;
            }
        }
        return false;
    }
})(jQuery);
