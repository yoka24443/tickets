(function ($) {
    'use strict';

    $(function () {
        $("#subBtn").click(function () {
            var loginName = $("#doc-ipt-email-1").val();
            var password = $("#doc-ipt-pwd-1").val();

            // if (loginName != '') {
            //     $('#error-modal').find(".am-modal-hd").text("请输入用户名！");
            //     $('#error-modal').modal('open');
            //     return;
            // }
            //
            // if (password != '') {
            //     $('#error-modal').find(".am-modal-hd").text("请输入密码！");
            //     $('#error-modal').modal('open');
            //     return;
            // }

            $.ajax({
                type: "POST",
                url: "/api/userLogin",    //向后端请求数据的url
                data: {
                    loginName: loginName,
                    password: password
                },
                success: function (data) {
                    console.log(data);
                    if (data == "1010") { //正常
                        $.ajax({
                            type: "GET",
                            url: "/api/getUserInfo",    //向后端请求数据的url
                            data: {
                                loginName: loginName
                            },
                            success: function (data) {
                                setCookie("loginName", data.userLoginName, "", "", "", "");
                                setCookie("userName", data.userName, "", "", "", "");
                                setCookie("userType", data.user.userType,"", "", "", "");
                                setCookie("userId", data.user.userId,"", "", "", "");
                                window.location.href = "userindex.html";
                            }
                        });
                    } else if (data == "1") { //账号被停用
                        $('#locked-modal').modal('open');
                    } else { //用户名或密码错误，用户不存在
                        $('#error-modal').modal('open');
                    }
                }
            });
        });

        function setCookie(name, value, expires, path, domain, secure) {
            document.cookie = name + "=" + encodeURI(value) +
                ((expires) ? "; expires=" + expires : "") +
                ((path) ? "; path=" + path : "") +
                ((domain) ? "; domain=" + domain : "") +
                ((secure) ? "; secure=" + secure : "");
        }

        //获取浏览器中的Cookie
        function getCookie(cName) {
            var cookieString = decodeURI(document.cookie);
            var cookieArray = cookieString.split("; ");
            // console.log(cookieArray.length);
            for (var i = 0; i < cookieArray.length; i++) {
                var cookieNum = cookieArray[i].split("=");
                // console.log(cookieNum.toString());
                var cookieName = cookieNum[0];
                var cookieValue = cookieNum[1];

                if (cookieName == cName) {
                    return cookieValue;
                }
            }
            return false;
        }
    });
})(jQuery);
