(function ($) {
    'use strict';

    $(function () {
        $("#subBtn").click(function () {
            var loginName = $("#doc-ipt-email-1").val();
            var password = $("#doc-ipt-pwd-1").val();
            var password2 = $("#doc-ipt-pwd-2").val();
            if (password == password2) {
                $.ajax({
                    type: "POST",
                    url: "/api/userRegister",    //向后端请求数据的url
                    data: {
                        userLoginName: loginName,
                        password: password
                    },
                    success: function (data) {
                        console.log(data);
                        if (data) {
                            if (data.userLoginName == "已存在") {
                                $('#user-modal').modal('open');
                                // $("#doc-ipt-email-1").val("");
                                $("#doc-ipt-pwd-1").val("");
                                $("#doc-ipt-pwd-2").val("");
                            } else {
                                $('#success-modal').modal('open');
                                setTimeout(function () {
                                    window.location.href = "userIndex.html";
                                }, 2000);
                            }
                        } else {
                            $('#error-modal').modal('open');
                        }
                    }
                });
            } else {
                $('#pwd-modal').modal('open');
            }
        });
    });
})(jQuery);