(function ($) {
    'use strict';
    $(document).ready(function () {

        $("#header").load('../public/header.html');
        var loginName = getCookie("loginName");
        var userName = getCookie("userName");
        var userType = getCookie("userType");
        var userId = getCookie("userId");

        if (userType == 0) {
            $("#sidebar").load('../public/adminsidebar.html');
        } else if(userType == 1) {
            $("#sidebar").load('../public/sidebar.html');
        } else if(userType == 2) {
            $("#sidebar").load('../public/customerSidebar.html');
        } else if(userType == 3) {
            $("#sidebar").load('../public/supplierSidebar.html');
        }

        // initData(loginName);

        $("#subBtn").click(function () {
            var userId = $("#userId").val();
            var password = $("#password").val();
            var newPassword = $("#newPassword").val();
            var rePassword = $("#rePassword").val();

            if (newPassword == undefined || rePassword == undefined || password == undefined) {
                $('#error-modal').find(".am-modal-hd").text("密码不能为空！");
                $('#error-modal').modal('open');
                return;
            }

            if (newPassword != rePassword) {
                $('#error-modal').find(".am-modal-hd").text("输入的新密码与重复新密码不一致！");
                $('#error-modal').modal('open');
                return;
            }

            $.ajax({
                type: "POST",
                url: "/api/changePwd",    //向后端请求数据的url
                data: {
                    loginName: loginName,
                    password: password,
                    newPassword: newPassword
                },
                success: function (data) {
                    if (data.status) {
                        $('#error-modal').find(".am-modal-hd").text(data.errorMessage);
                        $('#error-modal').modal('open');
                    } else {
                        $('#success-modal').modal('open');
                    }

                }
            });
        });
    });

    // function initData(loginName) {
    //     $.ajax({
    //         type: "GET",
    //         url: "/api/getUser",    //向后端请求数据的url
    //         data: {
    //             loginName: loginName
    //         },
    //         success: function (data) {
    //             // console.log(data);
    //             $("#userId").val(data.userId);
    //         }
    //     });
    // }

    //设置浏览器中的Cookie
    function setCookie(name, value, expires, path, domain, secure) {
        document.cookie = name + "=" + encodeURI(value) +
            ((expires) ? "; expires=" + expires : "") +
            ((path) ? "; path=" + path : "") +
            ((domain) ? "; domain=" + domain : "") +
            ((secure) ? "; secure=" + secure : "");
    }

    //获取浏览器中的Cookie
    function getCookie(cName) {
        var cookieString = decodeURI(document.cookie);
        var cookieArray = cookieString.split("; ");
        // console.log(cookieArray.length);
        for (var i = 0; i < cookieArray.length; i++) {
            var cookieNum = cookieArray[i].split("=");
            // console.log(cookieNum.toString());
            var cookieName = cookieNum[0];
            var cookieValue = cookieNum[1];

            if (cookieName == cName) {
                return cookieValue;
            }
        }
        return false;
    }
})(jQuery);
