(function ($) {
    'use strict';
    $(document).ready(function () {

        $("#header").load('../public/header.html');

        var loginName = getCookie("loginName");
        var userName = getCookie("userName");
        var userType = getCookie("userType");
        var userId = getCookie("userId");

        if (userType == 0) {
            $("#sidebar").load('../public/adminsidebar.html');
        } else if(userType == 1) {
            $("#sidebar").load('../public/sidebar.html');
        } else if(userType == 2) {
            $("#sidebar").load('../public/customerSidebar.html');
        } else if(userType == 3) {
            $("#sidebar").load('../public/supplierSidebar.html');
        }

        $("#subBtn").click(function () {
            var deviceNO = $("#deviceNO").val();
            var problem = $("#problem").val();
            var repairDate = $("#repairDate").val();
            var expressNO = $("#expressNO").val();

            $.ajax({
                type: "POST",
                url: "/api/addRepairOrder",    //向后端请求数据的url
                data: {
                    customerId: userId,
                    deviceNO: deviceNO,
                    problem: problem,
                    repairDate: repairDate,
                    expressNO: expressNO
                },
                success: function (data) {
                    console.log(data);
                    if (!data.status) {
                        $('#success-modal').modal('open');
                        $("#deviceNO").val("");
                        $("#problem").val("");
                        $("#repairDate").val("");
                        $("#expressNO").val("");
                    } else {
                        $('#error-modal').modal('open');
                    }

                }
            });
        });
    });

    //获取浏览器中的Cookie
    function getCookie(cName) {
        var cookieString = decodeURI(document.cookie);
        var cookieArray = cookieString.split("; ");
        // console.log(cookieArray.length);
        for (var i = 0; i < cookieArray.length; i++) {
            var cookieNum = cookieArray[i].split("=");
            // console.log(cookieNum.toString());
            var cookieName = cookieNum[0];
            var cookieValue = cookieNum[1];

            if (cookieName == cName) {
                return cookieValue;
            }
        }
        return false;
    }
})(jQuery);
