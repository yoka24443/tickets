(function ($) {
    'use strict';
    $(document).ready(function () {

        $("#header").load('../public/header.html');

        var loginName = getCookie("loginName");
        var userName = getCookie("userName");
        var userType = getCookie("userType");
        var userId = getCookie("userId");
        var repairId = getQueryVariable("repairId")

        if (userType == 0) {
            $("#sidebar").load('../public/adminsidebar.html');
        } else if(userType == 1) {
            $("#sidebar").load('../public/sidebar.html');
        } else if(userType == 2) {
            $("#sidebar").load('../public/customerSidebar.html');
        } else if(userType == 3) {
            $("#sidebar").load('../public/supplierSidebar.html');
        }

        $.ajax({
            type: "GET",
            url: "/api/getRepairOrderDetail",    //向后端请求数据的url
            data: {
                repairId: repairId
            },
            success: function (data) {
                // console.log(data);
                $("#repairId").val(data.data.repairId);
                $("#deviceNO").val(data.data.deviceNO);
                $("#problem").val(data.data.problem);
                $("#repairDate").val((new Date(data.data.repairDate)).toLocaleDateString().replace(new RegExp("/", "g"), "-"));
                $("#repairStatus").val(data.data.repairStatus == 0 ? "已报修" : "已反馈");
                $("#customerExpressNO").val(data.data.customerExpressNO);
                if(data.data.feedbackDate != null) {
                    $("#feedbackDate").val((new Date(data.data.feedbackDate)).toLocaleDateString().replace(new RegExp("/", "g"), "-"));
                }
                $("#feedbackAnswer").val(data.data.feedbackAnswer);
                $("#supplierExpressNO").val(data.data.supplierExpressNO);
            }
        });

        $("#subBtn").click(function () {
            var repairId = $("#repairId").val();
            var feedbackDate = $("#feedbackDate").val();
            var feedbackAnswer = $("#feedbackAnswer").val();
            var expressNO = $("#supplierExpressNO").val();

            $.ajax({
                type: "POST",
                url: "/api/addRepairFeedback",    //向后端请求数据的url
                data: {
                    repairId: repairId,
                    supplierId: userId,
                    feedbackDate: feedbackDate,
                    feedbackAnswer: feedbackAnswer,
                    expressNO: expressNO
                },
                success: function (data) {
                    // console.log(data);
                    if (!data.status) {
                        $('#success-modal').modal('open');
                        $("#feedbackAnswer").val("");
                        $("#feedbackDate").val("");
                        $("#expressNO").val("");
                    } else {
                        $('#error-modal').modal('open');
                    }
                }
            });
        });
    });

    //获取浏览器中的Cookie
    function getCookie(cName) {
        var cookieString = decodeURI(document.cookie);
        var cookieArray = cookieString.split("; ");
        // console.log(cookieArray.length);
        for (var i = 0; i < cookieArray.length; i++) {
            var cookieNum = cookieArray[i].split("=");
            // console.log(cookieNum.toString());
            var cookieName = cookieNum[0];
            var cookieValue = cookieNum[1];

            if (cookieName == cName) {
                return cookieValue;
            }
        }
        return false;
    }
    // 获取链接参数
    function getQueryVariable(variable)
    {
           var query = window.location.search.substring(1);
           var vars = query.split("&");
           for (var i=0;i<vars.length;i++) {
                   var pair = vars[i].split("=");
                   if(pair[0] == variable){return pair[1];}
           }
           return(false);
    }
})(jQuery);
