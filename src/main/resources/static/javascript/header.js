(function ($) {
    'use strict';
    $(function () {
        $(document).ready(function () {
            var loginName = getCookie("loginName");
            var userName = getCookie("userName");
            var userType = getCookie("userType");
            var userId = getCookie("userId");

            $(".tpl-logo").find("img").src = "../assets/i/logo_repair.png";

            if (loginName == false) {
                window.location.href = "login.html";
            } else {
                $("#username").text(userName);
            }

            $(".am-icon-sign-out .am-icon-power-off").parent().click(function() {
                setCookie("loginName", "", "", "", "", "");
                setCookie("userName", "", "", "", "", "");
                setCookie("userType", "", "", "", "", "");
                setCookie("userId", "", "", "", "", "");
                window.location.href = "login.html";
            });
        });
    });

    //获取浏览器中的Cookie
    function getCookie(cName) {
        var cookieString = decodeURI(document.cookie);
        var cookieArray = cookieString.split("; ");
        // console.log(cookieArray.length);
        for (var i = 0; i < cookieArray.length; i++) {
            var cookieNum = cookieArray[i].split("=");
            // console.log(cookieNum.toString());
            var cookieName = cookieNum[0];
            var cookieValue = cookieNum[1];

            if (cookieName == cName) {
                return cookieValue;
            }
        }
        return false;
    }

    function setCookie(name, value, expires, path, domain, secure) {
        document.cookie = name + "=" + encodeURI(value) +
            ((expires) ? "; expires=" + expires : "") +
            ((path) ? "; path=" + path : "") +
            ((domain) ? "; domain=" + domain : "") +
            ((secure) ? "; secure=" + secure : "");
    }
})(jQuery);
