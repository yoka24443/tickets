(function ($) {
    'use strict';
    $(document).ready(function () {

        $("#header").load('../public/header.html');

        var loginName = getCookie("loginName");
        var userName = getCookie("userName");
        var userType = getCookie("userType");
        var userId = getCookie("userId");

        if (userType == 0) {
            $("#sidebar").load('../public/adminsidebar.html');
        } else if(userType == 1) {
            $("#sidebar").load('../public/sidebar.html');
        } else if(userType == 2) {
            $("#sidebar").load('../public/customerSidebar.html');
        } else if(userType == 3) {
            $("#sidebar").load('../public/supplierSidebar.html');
        }

        initData(loginName);

        $("#subBtn").click(function () {
            var loginName = $("#loginName").val();
            var userName = $("#userName").val();
            var userType = $("#userType").val();
            var phone = $("#phone").val();
            var idCard = $("#idCard").val();
            var livePlace = $("#livePlace").val();
            var email = $("#email").val();

            $.ajax({
                type: "POST",
                url: "/api/updateUserInfo",    //向后端请求数据的url
                data: {
                    userLoginName: loginName,
                    userName: userName,
                    userType: userType,
                    userPhone: phone,
                    userLivePlace: livePlace,
                    userEmail: email,
                    userIdCard: idCard
                },
                success: function (data) {
                    console.log(data);
                    if (data) {
                        $('#success-modal').modal('open');
                        setCookie("userName", data.userName, "", "", "", "");
                    } else {
                        $('#error-modal').modal('open');
                    }

                }
            });
        });
    });

    function initData(loginName) {
        $.ajax({
            type: "GET",
            url: "/api/getUserInfo",    //向后端请求数据的url
            data: {
                loginName: loginName
            },
            success: function (data) {
                console.log(data);
                $("#detailId").val(data.detailId);
                $("#loginName").val(data.userLoginName);
                $("#userName").val(data.userName);
                $("#userType").val(data.userType);
                $("#phone").val(data.userPhone);
                $("#idCard").val(data.userIdCard);
                $("#livePlace").val(data.userLivePlace);
                $("#email").val(data.userEmail);
            }
        });
    }

    //设置浏览器中的Cookie
    function setCookie(name, value, expires, path, domain, secure) {
        document.cookie = name + "=" + encodeURI(value) +
            ((expires) ? "; expires=" + expires : "") +
            ((path) ? "; path=" + path : "") +
            ((domain) ? "; domain=" + domain : "") +
            ((secure) ? "; secure=" + secure : "");
    }

    //获取浏览器中的Cookie
    function getCookie(cName) {
        var cookieString = decodeURI(document.cookie);
        var cookieArray = cookieString.split("; ");
        // console.log(cookieArray.length);
        for (var i = 0; i < cookieArray.length; i++) {
            var cookieNum = cookieArray[i].split("=");
            // console.log(cookieNum.toString());
            var cookieName = cookieNum[0];
            var cookieValue = cookieNum[1];

            if (cookieName == cName) {
                return cookieValue;
            }
        }
        return false;
    }
})(jQuery);
