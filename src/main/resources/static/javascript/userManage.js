(function ($) {
    'use strict';
    $(document).ready(function () {

        $("#header").load('../public/header.html');

        var loginName = getCookie("loginName");
        var userName = getCookie("userName");
        var userType = getCookie("userType");
        var userId = getCookie("userId");

        if (userType == 0) {
            $("#sidebar").load('../public/adminsidebar.html');
        } else if(userType == 1) {
            $("#sidebar").load('../public/sidebar.html');
        } else if(userType == 2) {
            $("#sidebar").load('../public/customerSidebar.html');
        } else if(userType == 3) {
            $("#sidebar").load('../public/supplierSidebar.html');
        }

        initForm();

        $(".searchBtn").click(function () {
            var keywords = $("#searchText").val();
            $.ajax({
                type: "GET",
                url: "/api/keywordsUser",    //向后端请求数据的url
                data: {
                    keywords: keywords
                },
                success: function (data) {
                    if (data.length > 0) {
                        showForm(data);
                    }
                }
            });
        });

        function initForm() {
            var keywords = $("#searchText").val();
            $.ajax({
                type: "GET",
                url: "/api/user/list",    //向后端请求数据的url
                data: {
                    keywords: keywords
                },
                success: function (data) {
                    if (data.length > 0) {
                        showForm(data);
                    }
                }
            });
        }

        function deleteUser(userId) {
            // console.log(userId)
            $.ajax({
                type: "POST",
                url: "/api/user/delete",    //向后端请求数据的url
                data: {
                    userId: userId
                },
                success: function (data) {
                    // console.log(data);
                    if (!data.status) {
                        $('#delete-success-modal').modal('open');
                        initForm();
                    }
                }
            });
        }

        function showForm(data) {
            $("#formBody").empty();
            for (var i = 0; i < data.length; i++) {
                var dom = ' <tr>\n' +
                    '<td style="display: none" class="user_id">' + data[i].userId + '</td>\n' +
                    '<td>' + data[i].userLoginName + '</td>\n';
                    switch(data[i].userType) {
                        case 0:
                            dom += '<td>管理员</td>\n';
                            break;
                        case 1:
                            dom += '<td>普通用户</td>\n';
                            break;
                        case 2:
                            dom += '<td>客户用户</td>\n';
                            break;
                        case 3:
                            dom += '<td>供应商用户</td>\n';
                            break;
                        default:
                            dom += '<td>未知</td>\n';
                            break;
                    }

                    dom += '<td>' + (data[i].userStatus == "0" ? "正常" : data[i].userStatus == "1" ? "停用" : "删除") + '</td>\n' +
                    '<td>' + (new Date(data[i].lastLoginTime)).toJSON() + '</td>\n' +
                    '<td>\n' +
                    '<div class="am-btn-toolbar">\n' +
                    '<div class="am-btn-group am-btn-group-xs">\n' +
                    '<span class="am-btn am-btn-default am-btn-xs am-text-secondary editBtn"><span class="am-icon-pencil-square-o"></span> 编辑</span>\n';

                if (data[i].userLoginName != "admin") {
                    dom += '<span class="am-btn am-btn-default am-btn-xs am-text-danger am-hide-sm-only deleteBtn"><span class="am-icon-trash-o"></span> 删除</span>\n';
                }

                dom += '</div>\n' +
                    '</div>\n' +
                    '</td>\n' +
                    '</tr>';

                $("#formBody").append(dom);
            }

            $(".deleteBtn").click(function () {
                var userId = $(this).parent().parent().parent().prevAll().filter(".user_id").text();
                $('#delete-confirm').modal({
                    relatedTarget: this,
                    onConfirm: function (options) {
                        deleteUser(userId.toString());
                    },
                    // closeOnConfirm: false,
                    onCancel: function () {
                        return;
                    }
                });
            });

            $(".editBtn").click(function () {
                var userId = $(this).parent().parent().parent().prevAll().filter(".user_id").text();
                $.ajax({
                    type: "GET",
                    url: "/api/user/get",    //向后端请求数据的url
                    data: {
                        userId: userId
                    },
                    success: function (data) {
                        // console.log(data);
                        $("#userId").val(data.user.userId);
                        $("#loginName").text(data.user.userLoginName);
                        $("#userType").val(data.user.userType + "");
                        $("#userStatus").val(data.user.userStatus + "");
                        $("#lastLoginTime").text((new Date(data.user.lastLoginTime)).toJSON());
                        $("#registerTime").text((new Date(data.user.registerTime)).toJSON());
                        $("#userName").val(data.userName);
                        $("#gender").val(data.userGender);
                        $("#idCard").val(data.userIdCard);
                        $("#phone").val(data.userPhone);
                        $("#email").val(data.userEmail);
                        $("#livePlace").val(data.userLivePlace);

                        $('#update-user').modal({
                            relatedTarget: this,
                            onConfirm: function (options) {
                                var userId = $("#userId").val();
                                var userType_ = $("#userType").val();
                                var userStatus_ = $("#userStatus").val();
                                var userName_ = $("#userName").val();
                                var gender_ = $("#gender").val();
                                var idCard_ = $("#idCard").val();
                                var phone_ = $("#phone").val();
                                var email_ = $("#email").val();
                                var livePlace_ = $("#livePlace").val();
                                $.ajax({
                                    type: "POST",
                                    contentType: "application/x-www-form-urlencoded",
                                    url: "/api/user/update",    //向后端请求数据的url
                                    dataType: "JSON",
                                    data: {
                                        userId: userId,
                                        userType: userType_,
                                        userStatus: userStatus_,
                                        userName: userName_,
                                        gender: gender_,
                                        idCard: idCard_,
                                        phone: phone_,
                                        email: email_,
                                        livePlace: livePlace_
                                    },
                                    success: function (data) {
                                        if (data) {
                                            $('#update-success').modal('open');
                                            initForm();
                                        } else {
                                            $('#update-error').modal('open');
                                        }
                                    }
                                });
                            },
                            // closeOnConfirm: false,
                            onCancel: function () {
                                return;
                            }
                        });
                    }
                });
            });
        }
    });

//获取浏览器中的Cookie
    function getCookie(cName) {
        var cookieString = decodeURI(document.cookie);
        var cookieArray = cookieString.split("; ");
        // console.log(cookieArray.length);
        for (var i = 0; i < cookieArray.length; i++) {
            var cookieNum = cookieArray[i].split("=");
            // console.log(cookieNum.toString());
            var cookieName = cookieNum[0];
            var cookieValue = cookieNum[1];

            if (cookieName == cName) {
                return cookieValue;
            }
        }
        return false;
    }
})
(jQuery);
