package com.seventh.tickets.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "concert")
public class Concert implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "concert_id")
    private Long concertId;

    @Column(name = "concert_name")
    private String concertName;

    @Column(name = "concert_place")
    private String concertPlace;

    @Column(name = "concert_date")
    private Date concertDate;

    @Column(name = "concert_starttime")
    private Date concertStartTime;

    @Column(name = "singer")
    private String singer;

    public Long getConcertId() {
        return concertId;
    }

    public void setConcertId(Long concertId) {
        this.concertId = concertId;
    }

    public String getConcertName() {
        return concertName;
    }

    public void setConcertName(String concertName) {
        this.concertName = concertName;
    }

    public String getConcertPlace() {
        return concertPlace;
    }

    public void setConcertPlace(String concertPlace) {
        this.concertPlace = concertPlace;
    }

    public Date getConcertDate() {
        return concertDate;
    }

    public void setConcertDate(Date concertDate) {
        this.concertDate = concertDate;
    }

    public Date getConcertStartTime() {
        return concertStartTime;
    }

    public void setConcertStartTime(Date concertStartTime) {
        this.concertStartTime = concertStartTime;
    }

    public String getSinger() {
        return singer;
    }

    public void setSinger(String singer) {
        this.singer = singer;
    }

    @Override
    public String toString() {
        return "Concert{" +
                "concert_id=" + concertId +
                ", concertName='" + concertName + '\'' +
                ", concertPlace='" + concertPlace + '\'' +
                ", concertDate='" + concertDate + '\'' +
                ", concertStartTime='" + concertStartTime + '\'' +
                ", singer='" + singer + '\'' +
                '}';
    }
}
