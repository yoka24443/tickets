package com.seventh.tickets.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * RepairFeedback
 *
 * Created by yoka24443 on 2019/11/21.
 */
@Entity
@Table(name = "repair_feedback")
public class RepairFeedback implements Serializable{
    private static final long serialVersionUID = -2226008503485915564L;
    /**
     * 报修反馈Id
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long repairFeedbackId;

    /**
     * 报修单实体
     */
    @OneToOne
    @JoinColumn(name = "repair_id", referencedColumnName = "repair_id")
    private RepairOrder repairOrder;

    /**
     * 供应商用户Id
     */
    @Column(name = "supplier_id")
    private Long supplierId;

    /**
     * 反馈日期
     */
    @Column(name="feedback_date")
    private Date feedbackDate;

    /**
     * 反馈解答
     */
    @Column(name = "feedback_answer")
    private String feedbackAnswer;

    /**
     * 供应商快递单号
     */
    @Column(name = "supplier_express_no")
    private String supplierExpressNO;

    /**
     * 创建时间
     */
    @Column(name = "add_time")
    private Date addTime;

    public Long getRepairFeedbackId() {
        return repairFeedbackId;
    }

    public void setRepairFeedbackId(Long repairFeedbackId) {
        this.repairFeedbackId = repairFeedbackId;
    }

    public RepairOrder getRepairOrder() {
        return repairOrder;
    }

    public void setRepairOrder(RepairOrder repairOrder) {
        this.repairOrder = repairOrder;
    }

    public Long getSupplierId() {
        return supplierId;
    }

    public void setSupplierId(Long supplierId) {
        this.supplierId = supplierId;
    }

    public Date getFeedbackDate() {
        return feedbackDate;
    }

    public void setFeedbackDate(Date feedbackDate) {
        this.feedbackDate = feedbackDate;
    }

    public String getFeedbackAnswer() {
        return feedbackAnswer;
    }

    public void setFeedbackAnswer(String feedbackAnswer) {
        this.feedbackAnswer = feedbackAnswer;
    }

    public String getSupplierExpressNO() {
        return supplierExpressNO;
    }

    public void setSupplierExpressNO(String supplierExpressNO) {
        this.supplierExpressNO = supplierExpressNO;
    }

    public Date getAddTime() {
        return addTime;
    }

    public void setAddTime(Date addTime) {
        this.addTime = addTime;
    }
}
