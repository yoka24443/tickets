package com.seventh.tickets.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * 报修单
 * <p>
 * Created by yoka24443 on 2019/11/21.
 */
@Entity
@Table(name = "repair_order")
public class RepairOrder implements Serializable {
    private static final long serialVersionUID = -5777236439712212333L;
    /**
     * 报修Id
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "repair_id")
    private Long repairId;

    /**
     * 客户Id
     */
    @Column(name = "customer_id")
    private Long customerId;

    /**
     * 设备编号
     */
    @Column(name="device_no")
    private String deviceNO;

    /**
     * 问题描述
     */
    @Column(name = "problem")
    private String problem;

    /**
     * 报修日期
     */
    @Column(name = "repair_date")
    private Date repairDate;

    /**
     * 客户快递单号
     */
    @Column(name = "customer_express_no")
    private String customerExpressNO;

    /**
     * 报修状态
     */
    @Column(name = "repair_status")
    private Integer repairStatus;

    /**
     * 创建时间
     */
    @Column(name = "add_time")
    private Date addTime;

    public Long getRepairId() {
        return repairId;
    }

    public void setRepairId(Long repairId) {
        this.repairId = repairId;
    }

    public Long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Long customerId) {
        this.customerId = customerId;
    }

    public String getDeviceNO() {
        return deviceNO;
    }

    public void setDeviceNO(String deviceNO) {
        this.deviceNO = deviceNO;
    }

    public String getProblem() {
        return problem;
    }

    public void setProblem(String problem) {
        this.problem = problem;
    }

    public Date getRepairDate() {
        return repairDate;
    }

    public void setRepairDate(Date repairDate) {
        this.repairDate = repairDate;
    }

    public String getCustomerExpressNO() {
        return customerExpressNO;
    }

    public void setCustomerExpressNO(String customerExpressNO) {
        this.customerExpressNO = customerExpressNO;
    }

    public Integer getRepairStatus() {
        return repairStatus;
    }

    public void setRepairStatus(Integer repairStatus) {
        this.repairStatus = repairStatus;
    }

    public Date getAddTime() {
        return addTime;
    }

    public void setAddTime(Date addTime) {
        this.addTime = addTime;
    }
}
