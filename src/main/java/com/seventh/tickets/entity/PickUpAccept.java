package com.seventh.tickets.entity;

import javax.persistence.*;
import java.io.Serializable;

/**
 * 接机招待接受记录(PickUpAccept)实体类
 *
 * @author makejava
 * @since 2019-12-12 15:05:58
 */
@Entity
@Table(name="pick_up_accept")
public class PickUpAccept implements Serializable {
    private static final long serialVersionUID = 690677280959483408L;
    /**
    * 招待接受记录Id
    */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "accept_id")
    private Integer acceptId;
    /**
    * 接待人Id
    */
    @Column(name="accept_user_id")
    private Integer acceptUserId;
    /**
    *  接待人手机号
    */
    @Column(name="phone_number")
    private String phoneNumber;
    /**
    * app昵称
    */
    @Column(name="app_nick_name")
    private String appNickName;
    /**
    * 车辆型号
    */
    @Column(name="car_model")
    private String carModel;
    /**
    * 限定载客人数
    */
    @Column(name="limited_people_number")
    private Integer limitedPeopleNumber;


    public Integer getAcceptId() {
        return acceptId;
    }

    public void setAcceptId(Integer acceptId) {
        this.acceptId = acceptId;
    }

    public Integer getAcceptUserId() {
        return acceptUserId;
    }

    public void setAcceptUserId(Integer acceptUserId) {
        this.acceptUserId = acceptUserId;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getAppNickName() {
        return appNickName;
    }

    public void setAppNickName(String appNickName) {
        this.appNickName = appNickName;
    }

    public String getCarModel() {
        return carModel;
    }

    public void setCarModel(String carModel) {
        this.carModel = carModel;
    }

    public Integer getLimitedPeopleNumber() {
        return limitedPeopleNumber;
    }

    public void setLimitedPeopleNumber(Integer limitedPeopleNumber) {
        this.limitedPeopleNumber = limitedPeopleNumber;
    }

}