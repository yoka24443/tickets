package com.seventh.tickets.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * 接机申请记录(PickUpApply)实体类
 *
 * @author makejava
 * @since 2019-12-11 17:50:50
 */
@Entity
@Table(name = "pick_up_apply")
public class PickUpApply implements Serializable {
    private static final long serialVersionUID = 158149613932155755L;
    /**
     * 申请接机Id
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "apply_id")
    private Integer applyId;
    /**
     * 申请人Id
     */
    @Column(name = "apply_user_id")
    private Integer applyUserId;
    /**
     * app昵称
     */
    @Column(name = "app_nick_name")
    private String appNickName;
    /**
     * 同行人数
     */
    @Column(name = "people_number")
    private Integer peopleNumber;
    /**
     * 行李
     */
    @Column(name = "baggage")
    private String baggage;
    /**
     * 航班号
     */
    @Column(name = "flight_number")
    private String flightNumber;
    /**
     * 火车车次
     */
    @Column(name = "train_number")
    private String trainNumber;
    /**
     * 出发时间
     */
    @Column(name = "departure_time")
    private Date departureTime;
    /**
     * 到达时间
     */
    @Column(name = "arrival_time")
    private Date arrivalTime;
    /**
     * 出发城市
     */
    @Column(name = "departure_city")
    private String departureCity;
    /**
     * 到达城市
     */
    @Column(name = "arrival_city")
    private String arrivalCity;
    /**
     * 机场名称
     */
    @Column(name = "airport_name")
    private String airportName;
    /**
     * 火车站名称
     */
    @Column(name="train_station_name")
    private String trainStationName;
    /**
     * 详细接机地点
     */
    @Column(name = "pick_up_location")
    private String pickUpLocation;


    public Integer getApplyId() {
        return applyId;
    }

    public void setApplyId(Integer applyId) {
        this.applyId = applyId;
    }

    public Integer getApplyUserId() {
        return applyUserId;
    }

    public void setApplyUserId(Integer applyUserId) {
        this.applyUserId = applyUserId;
    }

    public String getAppNickName() {
        return appNickName;
    }

    public void setAppNickName(String appNickName) {
        this.appNickName = appNickName;
    }

    public Integer getPeopleNumber() {
        return peopleNumber;
    }

    public void setPeopleNumber(Integer peopleNumber) {
        this.peopleNumber = peopleNumber;
    }

    public String getBaggage() {
        return baggage;
    }

    public void setBaggage(String baggage) {
        this.baggage = baggage;
    }

    public String getFlightNumber() {
        return flightNumber;
    }

    public void setFlightNumber(String flightNumber) {
        this.flightNumber = flightNumber;
    }

    public String getTrainNumber() {
        return trainNumber;
    }

    public void setTrainNumber(String trainNumber) {
        this.trainNumber = trainNumber;
    }

    public Date getDepartureTime() {
        return departureTime;
    }

    public void setDepartureTime(Date departureTime) {
        this.departureTime = departureTime;
    }

    public Date getArrivalTime() {
        return arrivalTime;
    }

    public void setArrivalTime(Date arrivalTime) {
        this.arrivalTime = arrivalTime;
    }

    public String getDepartureCity() {
        return departureCity;
    }

    public void setDepartureCity(String departureCity) {
        this.departureCity = departureCity;
    }

    public String getArrivalCity() {
        return arrivalCity;
    }

    public void setArrivalCity(String arrivalCity) {
        this.arrivalCity = arrivalCity;
    }

    public String getAirportName() {
        return airportName;
    }

    public void setAirportName(String airportName) {
        this.airportName = airportName;
    }

    public String getTrainStationName() {
        return trainStationName;
    }

    public void setTrainStationName(String trainStationName) {
        this.trainStationName = trainStationName;
    }

    public String getPickUpLocation() {
        return pickUpLocation;
    }

    public void setPickUpLocation(String pickUpLocation) {
        this.pickUpLocation = pickUpLocation;
    }

}