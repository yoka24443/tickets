package com.seventh.tickets.entity;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "concert_ticket")
public class ConcertTicket implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ticket_id")
    private Long ticketId;

    @ManyToOne
    @JoinColumn(name = "concert_id", referencedColumnName = "concert_id")
    private Concert concert;

    @Column(name = "seat_grade")
    private String seatGrade;

    @Column(name = "seat_price")
    private Double seatPrice;

    @Column(name = "seat_total")
    private Integer seatTotal;

    @Column(name = "seat_left")
    private Integer seatLeft;

    public Long getTicketId() {
        return ticketId;
    }

    public void setTicketId(Long ticketId) {
        this.ticketId = ticketId;
    }

    public Concert getConcert() {
        return concert;
    }

    public void setConcert(Concert concert) {
        this.concert = concert;
    }

    public String getSeatGrade() {
        return seatGrade;
    }

    public void setSeatGrade(String seatGrade) {
        this.seatGrade = seatGrade;
    }

    public Double getSeatPrice() {
        return seatPrice;
    }

    public void setSeatPrice(Double seatPrice) {
        this.seatPrice = seatPrice;
    }

    public int getSeatTotal() {
        return seatTotal;
    }

    public void setSeatTotal(Integer seatTotal) {
        this.seatTotal = seatTotal;
    }

    public int getSeatLeft() {
        return seatLeft;
    }

    public void setSeatLeft(Integer seatLeft) {
        this.seatLeft = seatLeft;
    }
}
