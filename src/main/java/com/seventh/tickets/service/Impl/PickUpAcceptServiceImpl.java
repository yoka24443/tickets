package com.seventh.tickets.service.Impl;

import com.seventh.tickets.base.Result;
import com.seventh.tickets.service.PickUpAcceptService;
import com.seventh.tickets.web.vo.PickUpAcceptVO;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * PickUpAcceptServiceImpl
 *
 */
@Service
@Transactional
public class PickUpAcceptServiceImpl implements PickUpAcceptService {
    /**
     * 添加接机接待记录
     *
     * @param vo
     * @return
     */
    @Override
    public Result addPickUpAccept(PickUpAcceptVO vo) {
        return null;
    }

    /**
     * 获取接机接待记录
     *
     * @param applyId
     * @param acceptId
     * @return
     */
    @Override
    public Result getPickUpAccept(Integer applyId, Integer acceptId) {
        return null;
    }
}
