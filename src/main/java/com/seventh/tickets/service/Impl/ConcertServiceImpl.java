package com.seventh.tickets.service.Impl;

import com.seventh.tickets.base.Result;
import com.seventh.tickets.base.TransResult;
import com.seventh.tickets.entity.Concert;
import com.seventh.tickets.entity.ConcertTicket;
import com.seventh.tickets.enums.ConcertErrorCode;
import com.seventh.tickets.repository.ConcertRepository;
import com.seventh.tickets.repository.ConcertTicketRepository;
import com.seventh.tickets.service.ConcertService;
import com.seventh.tickets.web.util.DateUtil;
import com.seventh.tickets.web.vo.ConcertTicketVO;
import com.seventh.tickets.web.vo.ConcertVO;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

@Service
@Transactional
public class ConcertServiceImpl implements ConcertService {

    private final ConcertRepository concertRepository;
    private final ConcertTicketRepository concertTicketRepository;

    public ConcertServiceImpl(ConcertRepository concertRepository,
                              ConcertTicketRepository concertTicketRepository) {
        this.concertRepository = concertRepository;
        this.concertTicketRepository = concertTicketRepository;
    }

    @Override
    public Concert insertConcert(String concertName, String singer, String concertDate, String concertStartTime, String concertPlace, String grade_1, String price_1, String seat_1, String grade_2, String price_2, String seat_2, String grade_3, String price_3, String seat_3) throws Exception {

        if (concertName == null || singer == null || concertDate == null || concertStartTime == null || concertPlace == null || grade_1 == null || price_1 == null || seat_1 == null || grade_2 == null || price_2 == null || seat_2 == null || grade_3 == null || price_3 == null || seat_3 == null) {
            throw new Exception("新增演唱会信息失败，数据不完整");
        } else {
            Date _concertDate = DateUtil.getDate(String.format("%s 00:00:00", concertDate));
            Concert concert = concertRepository.findByConcertNameAndConcertDate(concertName, _concertDate);
            if (concert != null) {
                throw new Exception("新增演唱会信息失败,已经存在此演唱会");
            } else {
                Concert concert1 = new Concert();
                concert1.setConcertName(concertName);
                concert1.setSinger(singer);
                concert1.setConcertDate(_concertDate);
                concert1.setConcertStartTime(DateUtil.getDate(concertStartTime));
                concert1.setConcertPlace(concertPlace);
                concert1 = concertRepository.save(concert1);
//                System.out.println(concert1);
                if (concert1.getConcertId() > 0) {
                    ConcertTicket concertTicket = new ConcertTicket();
//                    System.out.println(concert1.getConcertId());
                    concertTicket.setConcert(concert1);
                    concertTicket.setSeatGrade(grade_1);
                    concertTicket.setSeatPrice(Double.parseDouble(price_1));
                    concertTicket.setSeatTotal(Integer.parseInt(seat_1));
                    concertTicket.setSeatLeft(Integer.parseInt(seat_1));
                    concertTicket = concertTicketRepository.save(concertTicket);

                    ConcertTicket concertTicket2 = new ConcertTicket();
                    concertTicket2.setConcert(concert1);
                    concertTicket2.setSeatGrade(grade_2);
                    concertTicket2.setSeatPrice(Double.parseDouble(price_2));
                    concertTicket2.setSeatTotal(Integer.parseInt(seat_2));
                    concertTicket2.setSeatLeft(Integer.parseInt(seat_2));
                    concertTicket2 = concertTicketRepository.save(concertTicket2);

                    ConcertTicket concertTicket3 = new ConcertTicket();
                    concertTicket3.setConcert(concert1);
                    concertTicket3.setSeatGrade(grade_3);
                    concertTicket3.setSeatPrice(Double.parseDouble(price_3));
                    concertTicket3.setSeatTotal(Integer.parseInt(seat_3));
                    concertTicket3.setSeatLeft(Integer.parseInt(seat_3));
                    concertTicket3 = concertTicketRepository.save(concertTicket3);

                    if (concertTicket.getConcert().getConcertId() > 0
                            && concertTicket2.getConcert().getConcertId() > 0
                            && concertTicket3.getConcert().getConcertId() > 0) {
                        return concert1;
                    } else {
                        throw new Exception("票务详细信息插入失败!");
                    }
                } else {
                    throw new Exception("插入失败!");
                }
            }
        }
    }

    @Override
    public Concert updateConcert(Long concertId, String concertName, String singer, String concertDate, String concertStartTime, String concertPlace) throws Exception {

        Concert concert = concertRepository.findOne(concertId);
        concert.setConcertName(concertName);
        concert.setSinger(singer);
        concert.setConcertDate(DateUtil.getDate(concertDate));
        concert.setConcertStartTime(DateUtil.getDate(concertStartTime));
        concert.setConcertPlace(concertPlace);
        concert = concertRepository.save(concert);
        if (concert.getConcertId() > 0) {
            return concert;
        } else {
            throw new Exception("演唱会信息修改失败!");
        }
    }

    @Override
    public String deleteConcert(Long concertId) throws Exception {
        Concert concert = concertRepository.findOne(concertId);
        if (concert == null) {
            throw new Exception("未查询到该演唱会信息");
        } else {
            List<ConcertTicket> concertTickets = concertTicketRepository.findAllByConcertId(concert.getConcertId());
            if (concertTickets.size() > 0) {
                for (int i = 0; i < concertTickets.size(); i++) {
                    concertTicketRepository.delete(concertTickets.get(i).getTicketId());
                }
                concertRepository.delete(concertId);
                return "0";
            } else {
                throw new Exception("未查询到该演唱会票务信息");
            }
        }
    }

    @Override
    public List<Concert> findAllConcerts() {
        return concertRepository.findAll();
    }

    @Override
    public List<Concert> findByKeywords(String keywords) throws Exception {
        if (keywords == null) {
            throw new Exception("查询条件不存在");
        } else {
            return concertRepository.findByKeywords(keywords);
        }
    }

    @Override
    public Concert findByConcertId(Long concertId) {
        return concertRepository.findOne(concertId);
    }

    /**
     * 添加演出信息
     *
     * @param concertVO
     * @return
     * @throws Exception
     */
    @Override
    public Result addConcert(ConcertVO concertVO) throws Exception {
        Result result = new TransResult();

        Map resultData = new HashMap();

        Concert concert;
        ConcertTicket ticket;
        List<ConcertTicket> ticketList;

        concert = new Concert();
        concert.setConcertName(concertVO.getConcertName());
        concert.setSinger(concertVO.getSinger());
        concert.setConcertDate(concertVO.getConcertDate());
        concert.setConcertStartTime(concertVO.getConcertStartTime());
        concert.setConcertPlace(concertVO.getConcertPlace());

        concert = concertRepository.save(concert);

        ticketList = new ArrayList<>();
        for (ConcertTicketVO ticketVO : concertVO.getTicketList()) {
            ticket = new ConcertTicket();
            ticket.setConcert(concert);
            ticket.setSeatGrade(ticketVO.getGrade());
            ticket.setSeatPrice(ticketVO.getPrice());
            ticket.setSeatTotal(ticketVO.getSeatTotal());
            ticket.setSeatLeft(ticketVO.getSeatTotal());

            ticketList.add(ticket);
        }

        ticketList = concertTicketRepository.save(ticketList);

        if (concert != null && ticketList.size() > 0) {
            resultData.put("concert", concert);
            resultData.put("ticketList", ticketList);
            result.setData(resultData);
        } else {
            result.setErrorCode(ConcertErrorCode.CONCERT_ADD_EXCEPTION);
        }

        return result;
    }
}
