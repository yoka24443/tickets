package com.seventh.tickets.service.Impl;

import com.seventh.tickets.entity.ConcertTicket;
import com.seventh.tickets.repository.ConcertTicketRepository;
import com.seventh.tickets.service.ConcertTicketService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class ConcertTicketServiceImpl implements ConcertTicketService {

    private final ConcertTicketRepository concertTicketRepository;

    public ConcertTicketServiceImpl(ConcertTicketRepository concertTicketRepository) {
        this.concertTicketRepository = concertTicketRepository;
    }

    @Override
    public List<ConcertTicket> findByConcertId(Long concertId) {
        return concertTicketRepository.findAllByConcertId(concertId);
    }

    @Override
    public ConcertTicket findPrice(Long concertId, String grade) {
        return concertTicketRepository.findByConcertIdAndSeatGrade(concertId, grade);
    }
}
