package com.seventh.tickets.service.Impl;

import com.seventh.tickets.base.Result;
import com.seventh.tickets.base.TransResult;
import com.seventh.tickets.entity.RepairFeedback;
import com.seventh.tickets.entity.RepairOrder;
import com.seventh.tickets.enums.RepairErrorCode;
import com.seventh.tickets.enums.RepairStatus;
import com.seventh.tickets.repository.RepairFeedbackRepository;
import com.seventh.tickets.repository.RepairOrderRepository;
import com.seventh.tickets.service.RepairFeedbackService;
import com.seventh.tickets.web.vo.RepairFeedbackVO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * RepairFeedbackServiceImpl
 *
 * Created by yoka24443 on 2019/11/21.
 */
@Service
@Transactional
public class RepairFeedbackServiceImpl implements RepairFeedbackService{

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    private final RepairOrderRepository repairOrderRepository;
    private final RepairFeedbackRepository repairFeedbackRepository;

    public RepairFeedbackServiceImpl(RepairOrderRepository repairOrderRepository, RepairFeedbackRepository repairFeedbackRepository) {
        this.repairOrderRepository = repairOrderRepository;
        this.repairFeedbackRepository = repairFeedbackRepository;
    }

    /**
     * 添加报修反馈
     *
     * @param vo
     * @return
     */
    @Override
    public Result addRepairFeedback(RepairFeedbackVO vo) throws Exception {
        Result result = new TransResult();

        RepairOrder repairOrder = repairOrderRepository.getOne(vo.getRepairId());

        if(repairOrder == null) {
            logger.info(String.format("错误: %s, 报修单id为%s", RepairErrorCode.REPAIR_ORDER_IS_NOT_EXISTS.getText(), vo.getRepairId()));
            result.setErrorCode(RepairErrorCode.REPAIR_ORDER_IS_NOT_EXISTS);

            return result;
        } else if(repairOrder.getRepairStatus().equals(RepairStatus.IS_FEEDBACK.getStatus())) {
            logger.info(String.format("错误: %s, 报修单id为%s", RepairErrorCode.REPAIR_ORDER_IS_REPAIRED.getText(), vo.getRepairId()));
            result.setErrorCode(RepairErrorCode.REPAIR_ORDER_IS_REPAIRED);

            return result;
        }

        RepairFeedback entity = new RepairFeedback();
        entity.setRepairOrder(repairOrder);
        entity.setSupplierId(vo.getSupplierId());
        entity.setFeedbackDate(vo.getFeedbackDate());
        entity.setFeedbackAnswer(vo.getFeedbackAnswer());
        entity.setSupplierExpressNO(vo.getExpressNO());

        entity = repairFeedbackRepository.save(entity);

        // 报修状态置为已反馈
        repairOrder.setRepairStatus(RepairStatus.IS_FEEDBACK.getStatus());
        repairOrder = repairOrderRepository.save(repairOrder);

        entity.setRepairOrder(repairOrder);

        if(entity != null) {
            result.setData(entity);
        }

        return result;
    }

    /**
     * 获取报修单反馈
     *
     * @param repairId
     * @return
     * @throws Exception
     */
    @Override
    public Result getRepairFeedback(Long repairId, Long repairFeedbackId) throws Exception {
        Result result = new TransResult();

        RepairFeedback entity = repairFeedbackRepository.getRepairFeedback(repairId, repairFeedbackId);

        if(entity != null) {
            result.setData(entity);
        }

        return result;
    }
}
