package com.seventh.tickets.service.Impl;

import com.seventh.tickets.base.Result;
import com.seventh.tickets.service.PickUpApplyService;
import com.seventh.tickets.web.vo.PickUpApplyVO;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * PickUpAcceptServiceImpl
 *
 */
@Service
@Transactional
public class PickUpApplyServiceImpl implements PickUpApplyService {
    /**
     * 添加接机申请
     *
     * @return
     */
    @Override
    public Result addPickUpApply(PickUpApplyVO vo) {
        return null;
    }

    /**
     * 查询接机申请记录
     *
     * @return
     */
    @Override
    public Result findPickUpApply(PickUpApplyVO vo) {
        return null;
    }

    /**
     * 查询接机申请记录
     *
     * @return
     */
    @Override
    public Result findAllPickUpApply(PickUpApplyVO vo) {
        return null;
    }

    /**
     * 获取接机申请记录详情(包含接机接待详情)
     *
     * @return
     */
    @Override
    public Result getPickUpdateApplyDetail(PickUpApplyVO vo) {
        return null;
    }
}
