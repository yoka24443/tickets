package com.seventh.tickets.service.Impl;

import com.seventh.tickets.base.Result;
import com.seventh.tickets.base.TransResult;
import com.seventh.tickets.entity.RepairFeedback;
import com.seventh.tickets.entity.RepairOrder;
import com.seventh.tickets.entity.User;
import com.seventh.tickets.entity.UserDetail;
import com.seventh.tickets.enums.RepairStatus;
import com.seventh.tickets.repository.RepairFeedbackRepository;
import com.seventh.tickets.repository.RepairOrderRepository;
import com.seventh.tickets.repository.UserDetailRepository;
import com.seventh.tickets.repository.UserRepository;
import com.seventh.tickets.service.RepairOrderService;
import com.seventh.tickets.service.dto.RepairOrderDetailDTO;
import com.seventh.tickets.web.vo.RepairOrderVO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * RepairOrderServiceImpl
 *
 * Created by yoka24443 on 2019/11/21.
 */
@Service
@Transactional
public class RepairOrderServiceImpl implements RepairOrderService {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    private final RepairOrderRepository repairOrderRepository;
    private final RepairFeedbackRepository repairFeedbackRepository;
    private final UserDetailRepository userDetailRepository;

    public RepairOrderServiceImpl(RepairOrderRepository repairOrderRepository, RepairFeedbackRepository repairFeedbackRepository, UserDetailRepository userDetailRepository) {
        this.repairOrderRepository = repairOrderRepository;
        this.repairFeedbackRepository = repairFeedbackRepository;
        this.userDetailRepository = userDetailRepository;
    }

    @Override
    public Result addRepairOrder(RepairOrderVO vo) {
        Result result = new TransResult();

        RepairOrder entity = new RepairOrder();
        entity.setCustomerId(vo.getCustomerId());
        entity.setDeviceNO(vo.getDeviceNO());
        entity.setProblem(vo.getProblem());
        entity.setRepairDate(vo.getRepairDate());
        entity.setCustomerExpressNO(vo.getExpressNO());
        // 报修状态置为已报修
        entity.setRepairStatus(RepairStatus.IS_REPAIRED.getStatus());

        entity = repairOrderRepository.save(entity);

        if(entity != null) {
            result.setData(entity);
        }

        return result;
    }

    /**
     * 查询报修单
     *
     * @param vo
     * @return
     */
    @Override
    public Result findRepairOrder(RepairOrderVO vo) {
        Result result = new TransResult();

        RepairOrder entity = new RepairOrder();
        entity.setCustomerId(vo.getCustomerId());
        Example<RepairOrder> example = Example.of(entity);

        List<RepairOrder> list = repairOrderRepository.findAll(example);

        result.setData(list);

        return result;
    }

    /**
     * 查询报修单
     *
     * @param vo
     * @return
     */
    @Override
    public Result findAllRepairOrder(RepairOrderVO vo) {
        Result result = new TransResult();

        List<RepairOrder> list = repairOrderRepository.findAllByVO(vo.getDeviceNO(), vo.getProblem(),
                                        vo.getExpressNO(), vo.getStartDate(), vo.getEndDate(), vo.getStatus());

        if(list != null && list.size() > 0) {
            result.setData(list);
        }

        return result;
    }

    /**
     * 获取报修单详情(包含报修反馈)
     *
     * @param repairId
     * @return
     */
    @Override
    public Result getRepairOrderDetail(Long repairId) {
        Result result = new TransResult();
        RepairOrderDetailDTO dto = new RepairOrderDetailDTO();

        RepairOrder repairOrder = repairOrderRepository.getOne(repairId);
        UserDetail customer = userDetailRepository.getOne(repairOrder.getCustomerId());

        dto.setRepairId(repairOrder.getRepairId());
        dto.setCustomerId(repairOrder.getCustomerId());
        dto.setCustomerName(customer.getUserName());
        dto.setDeviceNO(repairOrder.getDeviceNO());
        dto.setProblem(repairOrder.getProblem());
        dto.setRepairDate(repairOrder.getRepairDate());
        dto.setRepairStatus(repairOrder.getRepairStatus());
        dto.setCustomerExpressNO(repairOrder.getCustomerExpressNO());

        if(repairOrder != null && repairOrder.getRepairStatus().equals(RepairStatus.IS_FEEDBACK.getStatus())) {
            RepairFeedback feedback = new RepairFeedback();
            feedback.setRepairOrder(repairOrder);
            Example<RepairFeedback> example = Example.of(feedback);
            RepairFeedback repairFeedback = repairFeedbackRepository.findOne(example);
            if(repairFeedback != null) {
                UserDetail supplier = userDetailRepository.getOne(repairFeedback.getSupplierId());

                dto.setSupplierId(repairFeedback.getSupplierId());
                dto.setSupplierName(supplier.getUserName());
                dto.setRepairFeedbackId(repairFeedback.getRepairFeedbackId());
                dto.setFeedbackDate(repairFeedback.getFeedbackDate());
                dto.setFeedbackAnswer(repairFeedback.getFeedbackAnswer());
                dto.setSupplierExpressNO(repairFeedback.getSupplierExpressNO());
            }
        }

        result.setData(dto);

        return result;
    }
}
