package com.seventh.tickets.service;

import com.seventh.tickets.entity.ConcertTicket;

import java.util.List;

/**
 * ConcertTicketService
 */
public interface ConcertTicketService {

    /**
     * 查询演出票务信息
     *
     * @param concertId
     * @return
     */
    List<ConcertTicket> findByConcertId(Long concertId);

    /**
     * 查询演出票价
     *
     * @param concertId
     * @param grade
     * @return
     */
    ConcertTicket findPrice(Long concertId, String grade);
}
