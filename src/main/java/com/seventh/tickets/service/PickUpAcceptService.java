package com.seventh.tickets.service;

import com.seventh.tickets.base.Result;
import com.seventh.tickets.web.vo.PickUpAcceptVO;

/**
 * PickUpAcceptService
 *
 */
public interface PickUpAcceptService {

    /**
     * 添加接机接待记录
     *
     * @param vo
     * @return
     */
    Result addPickUpAccept(PickUpAcceptVO vo);

    /**
     * 获取接机接待记录
     *
     * @param applyId
     * @param acceptId
     * @return
     */
    Result getPickUpAccept(Integer applyId, Integer acceptId);
}
