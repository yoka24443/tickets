package com.seventh.tickets.service;

import com.seventh.tickets.base.Result;
import com.seventh.tickets.web.vo.RepairFeedbackVO;

/**
 * RepairFeedbackService
 *
 * Created by yoka24443 on 2019/11/21.
 */
public interface RepairFeedbackService {
    /**
     * 添加报修反馈
     *
     * @param vo
     * @return
     * @throws Exception
     */
    Result addRepairFeedback(RepairFeedbackVO vo) throws Exception;

    /**
     * 获取报修单反馈
     *
     * @param repairId
     * @return
     * @throws Exception
     */
    Result getRepairFeedback(Long repairId, Long repairFeedbackId) throws Exception;
}
