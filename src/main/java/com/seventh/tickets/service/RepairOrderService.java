package com.seventh.tickets.service;

import com.seventh.tickets.base.Result;
import com.seventh.tickets.web.vo.RepairOrderVO;

/**
 * RepairOrderService
 *
 * Created by yoka24443 on 2019/11/21.
 */
public interface RepairOrderService {

    /**
     * 添加报修单
     *
     * @param vo
     * @return
     */
    Result addRepairOrder(RepairOrderVO vo);

    /**
     * 查询报修单
     *
     * @param vo
     * @return
     */
    Result findRepairOrder(RepairOrderVO vo);

    /**
     * 查询报修单
     *
     * @param vo
     * @return
     */
    Result findAllRepairOrder(RepairOrderVO vo);

    /**
     * 获取报修单详情(包含报修反馈)
     *
     * @param repairId
     * @return
     */
    Result getRepairOrderDetail(Long repairId);
}
