package com.seventh.tickets.service.dto;

import com.seventh.tickets.base.BaseDTO;

import java.io.Serializable;
import java.util.Date;

/**
 * RepairOrderDetailDTO
 *
 * Created by yoka24443 on 2019/11/27.
 */
public class RepairOrderDetailDTO implements BaseDTO, Serializable {
    private static final long serialVersionUID = -136886509301985514L;

    /**
     * 报修单Id
     */
    private Long repairId;

    /**
     * 报修反馈Id
     */
    private Long repairFeedbackId;

    /**
     * 客户Id
     */
    private Long customerId;

    /**
     * 客户名称
     */
    private String customerName;

    /**
     * 供应商Id
     */
    private Long supplierId;

    /**
     * 供应商名称
     */
    private String supplierName;

    /**
     * 设备号
     */
    private String deviceNO;

    /**
     * 问题描述
     */
    private String problem;

    /**
     * 报修日期
     */
    private Date repairDate;

    /**
     * 客户快递单号
     */
    private String customerExpressNO;

    /**
     * 报修状态
     */
    private Integer repairStatus;

    /**
     * 反馈日期
     */
    private Date feedbackDate;

    /**
     * 反馈解答
     */
    private String feedbackAnswer;

    /**
     * 供应商快递单号
     */
    private String supplierExpressNO;

    public Long getRepairId() {
        return repairId;
    }

    public void setRepairId(Long repairId) {
        this.repairId = repairId;
    }

    public Long getRepairFeedbackId() {
        return repairFeedbackId;
    }

    public void setRepairFeedbackId(Long repairFeedbackId) {
        this.repairFeedbackId = repairFeedbackId;
    }

    public Long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Long customerId) {
        this.customerId = customerId;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public Long getSupplierId() {
        return supplierId;
    }

    public void setSupplierId(Long supplierId) {
        this.supplierId = supplierId;
    }

    public String getSupplierName() {
        return supplierName;
    }

    public void setSupplierName(String supplierName) {
        this.supplierName = supplierName;
    }

    public String getDeviceNO() {
        return deviceNO;
    }

    public void setDeviceNO(String deviceNO) {
        this.deviceNO = deviceNO;
    }

    public String getProblem() {
        return problem;
    }

    public void setProblem(String problem) {
        this.problem = problem;
    }

    public Date getRepairDate() {
        return repairDate;
    }

    public void setRepairDate(Date repairDate) {
        this.repairDate = repairDate;
    }

    public String getCustomerExpressNO() {
        return customerExpressNO;
    }

    public void setCustomerExpressNO(String customerExpressNO) {
        this.customerExpressNO = customerExpressNO;
    }

    public Integer getRepairStatus() {
        return repairStatus;
    }

    public void setRepairStatus(Integer repairStatus) {
        this.repairStatus = repairStatus;
    }

    public Date getFeedbackDate() {
        return feedbackDate;
    }

    public void setFeedbackDate(Date feedbackDate) {
        this.feedbackDate = feedbackDate;
    }

    public String getFeedbackAnswer() {
        return feedbackAnswer;
    }

    public void setFeedbackAnswer(String feedbackAnswer) {
        this.feedbackAnswer = feedbackAnswer;
    }

    public String getSupplierExpressNO() {
        return supplierExpressNO;
    }

    public void setSupplierExpressNO(String supplierExpressNO) {
        this.supplierExpressNO = supplierExpressNO;
    }
}
