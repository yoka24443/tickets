package com.seventh.tickets.service.dto;

import com.seventh.tickets.base.BaseDTO;

import java.io.Serializable;

/**
 * ConcertTicketDTO
 * <p>
 * Created by yoka24443 on 2018/12/6.
 */
public class ConcertTicketDTO implements BaseDTO, Serializable {
    private static final long serialVersionUID = 4780190025995417549L;
    /**
     * 演唱会席位票主键
     */
    private Long ticketId;
    /**
     * 演唱会Id
     */
    private Long concertId;
    /**
     * 席位级别
     */
    private String seatGrade;
    /**
     * 席位价格
     */
    private Double seatPrice;
    /**
     * 席位总数量
     */
    private Integer seatTotal;
    /**
     * 席位剩余数量
     */
    private Integer seatLeft;

    public Long getTicketId() {
        return ticketId;
    }

    public void setTicketId(Long ticketId) {
        this.ticketId = ticketId;
    }

    public Long getConcertId() {
        return concertId;
    }

    public void setConcertId(Long concertId) {
        this.concertId = concertId;
    }

    public String getSeatGrade() {
        return seatGrade;
    }

    public void setSeatGrade(String seatGrade) {
        this.seatGrade = seatGrade;
    }

    public Double getSeatPrice() {
        return seatPrice;
    }

    public void setSeatPrice(Double seatPrice) {
        this.seatPrice = seatPrice;
    }

    public Integer getSeatTotal() {
        return seatTotal;
    }

    public void setSeatTotal(Integer seatTotal) {
        this.seatTotal = seatTotal;
    }

    public Integer getSeatLeft() {
        return seatLeft;
    }

    public void setSeatLeft(Integer seatLeft) {
        this.seatLeft = seatLeft;
    }
}
