package com.seventh.tickets.service;

import com.seventh.tickets.base.Result;
import com.seventh.tickets.entity.User;

import java.util.List;

/**
 * UserService
 */
public interface UserService {

    /**
     * 用户登录
     *
     * @param userLoginName
     * @param password
     * @return
     * @throws Exception
     */
    String userLogin(String userLoginName, String password) throws Exception;

    /**
     * 用户注册
     *
     * @param userLoginName
     * @param password
     * @return
     * @throws Exception
     */
    User userRegister(String userLoginName, String password) throws Exception;

    /**
     * 修改密码
     *
     * @param userLoginName
     * @param password
     * @param newPassword
     * @return
     * @throws Exception
     */
    Result changePassword(String userLoginName, String password, String newPassword) throws Exception;

    /**
     * 获取用户集合
     *
     * @param keywords
     * @return
     * @throws Exception
     */
    List<User> listUser(String keywords) throws Exception;

    /**
     * 修改用户信息
     *
     * @param user
     * @return
     * @throws Exception
     */
    User updateUser(User user) throws Exception;

    /**
     * 获取用户信息
     *
     * @param userId
     * @return
     * @throws Exception
     */
    User getUser(Long userId) throws Exception;

    /**
     * 获取用户信息
     *
     * @param loginName
     * @return
     * @throws Exception
     */
    User getUser(String loginName) throws Exception;

    /**
     * 删除用户
     *
     * @param userId
     * @return
     * @throws Exception
     */
    Result deleteUser(Long userId) throws Exception;
}
