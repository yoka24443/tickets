package com.seventh.tickets.service;

import com.seventh.tickets.entity.UserDetail;

/**
 * UserDetailService
 */
public interface UserDetailService {

    /**
     * 获取用户详细信息
     *
     * @param userId
     * @return
     */
    UserDetail getUserById(Long userId);

    /**
     * 获取用户详细信息
     *
     * @param userLoginName
     * @return
     */
    UserDetail getUserDetail(String userLoginName);

    /**
     * 更新用户详细信息
     *
     * @param userLoginName
     * @param username
     * @param userPhone
     * @param userLivePlace
     * @param userEmail
     * @param userIdCard
     * @return
     * @throws Exception
     */
    UserDetail updateUserDetail(String userLoginName, String username, String userPhone, String userLivePlace, String userEmail, String userIdCard) throws Exception;

    /**
     * 更新用户详细信息
     *
     * @param entity UserDetail
     * @return
     */
    UserDetail updateUserDetail(UserDetail entity);
}
