package com.seventh.tickets.service;

import com.seventh.tickets.base.Result;
import com.seventh.tickets.entity.Concert;
import com.seventh.tickets.web.vo.ConcertVO;

import java.util.List;

/**
 * ConcertService
 */
public interface ConcertService {

    /**
     * 添加演出信息
     *
     * @param concertName
     * @param singer
     * @param concertDate
     * @param concertStartTime
     * @param concertPlace
     * @param grade_1
     * @param price_1
     * @param seat_1
     * @param grade_2
     * @param price_2
     * @param seat_2
     * @param grade_3
     * @param price_3
     * @param seat_3
     * @return
     * @throws Exception
     */
    Concert insertConcert(String concertName, String singer, String concertDate, String concertStartTime, String concertPlace, String grade_1, String price_1, String seat_1, String grade_2, String price_2, String seat_2, String grade_3, String price_3, String seat_3) throws Exception;

    /**
     * 添加演出信息
     *
     * @param concertVO
     * @return
     * @throws Exception
     */
    Result addConcert(ConcertVO concertVO) throws Exception;

    /**
     * 更新演出信息
     *
     * @param concertId
     * @param concertName
     * @param singer
     * @param concertDate
     * @param concertStartTime
     * @param concertPlace
     * @return
     * @throws Exception
     */
    Concert updateConcert(Long concertId, String concertName, String singer, String concertDate, String concertStartTime, String concertPlace) throws Exception;

    /**
     * 删除演出信息
     *
     * @param concertId
     * @return
     * @throws Exception
     */
    String deleteConcert(Long concertId) throws Exception;

    /**
     * 查询演出信息
     *
     * @return
     */
    List<Concert> findAllConcerts();

    /**
     * 查询演出信息(关键字)
     *
     * @param keywords
     * @return
     * @throws Exception
     */
    List<Concert> findByKeywords(String keywords) throws Exception;

    /**
     * 查询演出信息(ByConcertId)
     *
     * @param concertId
     * @return
     */
    Concert findByConcertId(Long concertId);

}
