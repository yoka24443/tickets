package com.seventh.tickets.service;

import com.seventh.tickets.base.Result;
import com.seventh.tickets.entity.Booking;

import java.util.List;

/**
 * BookingService
 */
public interface BookingService {

    /**
     * 添加订票信息
     *
     * @param concertId
     * @param userId
     * @param userIdCard
     * @param seatType
     * @return
     * @throws Exception
     */
    Result insertBooking(Long concertId, Long userId, String userIdCard, String seatType) throws Exception;

    /**
     * 查询订票记录(ByUserIdCard)
     *
     * @param userIdCard
     * @return
     */
    List<Booking> findByUserIdCard(String userIdCard);

    /**
     * 查询订票记录(ByUserId)
     *
     * @param userId
     * @return
     */
    List<Booking> findByUserId(Long userId);

    /**
     * 退票
     *
     * @param bookingId
     * @param concertId
     * @param userId
     * @return
     */
    Result refundBooking(Long bookingId, Long concertId, Long userId);
}
