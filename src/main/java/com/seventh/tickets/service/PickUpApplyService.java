package com.seventh.tickets.service;

import com.seventh.tickets.base.Result;
import com.seventh.tickets.web.vo.PickUpApplyVO;

/**
 * PickUpApplyService
 *
 */
public interface PickUpApplyService {
    /**
     * 添加接机申请
     *
     * @return
     */
    Result addPickUpApply(PickUpApplyVO vo);

    /**
     * 查询接机申请记录
     *
     * @return
     */
    Result findPickUpApply(PickUpApplyVO vo);

    /**
     * 查询接机申请记录
     *
     * @return
     */
    Result findAllPickUpApply(PickUpApplyVO vo);

    /**
     * 获取接机申请记录详情(包含接机接待详情)
     *
     * @return
     */
    Result getPickUpdateApplyDetail(PickUpApplyVO vo);
}
