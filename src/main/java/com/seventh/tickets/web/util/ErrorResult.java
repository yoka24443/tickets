package com.seventh.tickets.web.util;


import com.alibaba.fastjson.JSON;

import java.time.ZonedDateTime;

public class ErrorResult {
    private String errorMessage;
    private StackTraceElement[] traceElements;
    private String origin;
    private String extraData;
    private ZonedDateTime time = ZonedDateTime.now();

    public ErrorResult() {
    }

    public ErrorResult(Exception e) {
        this.errorMessage = e.getMessage();
        this.traceElements = e.getStackTrace();
        this.origin = e.getClass().toString();
        this.extraData = JSON.toJSONString(e.toString());

    }

    public ErrorResult(String msg) {
        this.errorMessage = msg;
    }

    @Override
    public String toString() {
        return JSON.toJSONString(this);
    }


    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public StackTraceElement[] getTraceElements() {
        return traceElements;
    }

    public void setTraceElements(StackTraceElement[] traceElements) {
        this.traceElements = traceElements;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getExtraData() {
        return extraData;
    }

    public void setExtraData(String extraData) {
        this.extraData = extraData;
    }

    public ZonedDateTime getTime() {
        return time;
    }
}
