package com.seventh.tickets.web.vo;

import com.seventh.tickets.base.BaseVO;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * PickUpAcceptVO
 *
 */
public class PickUpAcceptVO implements BaseVO, Serializable {
    private static final long serialVersionUID = 2955719236672767515L;

    /**
     * 接待人Id
     */
    @NotNull
    private Integer userId;
    /**
     * 接机申请Id
     */
    @NotNull
    private Integer applyId;
    /**
     * 接待人手机号
     */
    private String phoneNumber;
    /**
     * 接待人App昵称
     */
    private String nickName;
    /**
     * 车辆型号
     */
    private String carModel;
    /**
     * 限制载客人数
     */
    private Integer limitedPeopleNumber;

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getApplyId() {
        return applyId;
    }

    public void setApplyId(Integer applyId) {
        this.applyId = applyId;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getCarModel() {
        return carModel;
    }

    public void setCarModel(String carModel) {
        this.carModel = carModel;
    }

    public Integer getLimitedPeopleNumber() {
        return limitedPeopleNumber;
    }

    public void setLimitedPeopleNumber(Integer limitedPeopleNumber) {
        this.limitedPeopleNumber = limitedPeopleNumber;
    }
}
