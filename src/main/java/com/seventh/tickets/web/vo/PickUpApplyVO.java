package com.seventh.tickets.web.vo;

import com.seventh.tickets.base.BaseVO;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

/**
 * PickUpApplyVO
 *
 */
public class PickUpApplyVO implements BaseVO, Serializable {
    private static final long serialVersionUID = -1607863357062538598L;

    /**
     * 接机申请人
     */
    @NotNull
    private Integer userId;

    /**
     * 申请人app昵称
     */
    private String nickName;

    /**
     * 同行人数
     */
    @NotNull
    private Integer peopleNumber;

    /**
     * 行李
     */
    private String baggage;

    /**
     * 航班号
     */
    private String flightNumber;

    /**
     * 火车班次号
     */
    private String trainNumber;

    /**
     * 出发时间
     */
    @NotNull
    private Date departureTime;

    /**
     * 出发城市
     */
    private String departureCity;

    /**
     * 到达时间
     */
    @NotNull
    private Date arrivalTime;

    /**
     * 到达城市
     */
    private String arrivalCity;

    /**
     * 机场名称
     */
    private String airportName;

    /**
     * 火车站名称
     */
    private String trainStationName;

    /**
     * 接机地点
     */
    private String location;

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public Integer getPeopleNumber() {
        return peopleNumber;
    }

    public void setPeopleNumber(Integer peopleNumber) {
        this.peopleNumber = peopleNumber;
    }

    public String getBaggage() {
        return baggage;
    }

    public void setBaggage(String baggage) {
        this.baggage = baggage;
    }

    public String getFlightNumber() {
        return flightNumber;
    }

    public void setFlightNumber(String flightNumber) {
        this.flightNumber = flightNumber;
    }

    public String getTrainNumber() {
        return trainNumber;
    }

    public void setTrainNumber(String trainNumber) {
        this.trainNumber = trainNumber;
    }

    public Date getDepartureTime() {
        return departureTime;
    }

    public void setDepartureTime(Date departureTime) {
        this.departureTime = departureTime;
    }

    public String getDepartureCity() {
        return departureCity;
    }

    public void setDepartureCity(String departureCity) {
        this.departureCity = departureCity;
    }

    public Date getArrivalTime() {
        return arrivalTime;
    }

    public void setArrivalTime(Date arrivalTime) {
        this.arrivalTime = arrivalTime;
    }

    public String getArrivalCity() {
        return arrivalCity;
    }

    public void setArrivalCity(String arrivalCity) {
        this.arrivalCity = arrivalCity;
    }

    public String getAirportName() {
        return airportName;
    }

    public void setAirportName(String airportName) {
        this.airportName = airportName;
    }

    public String getTrainStationName() {
        return trainStationName;
    }

    public void setTrainStationName(String trainStationName) {
        this.trainStationName = trainStationName;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }
}
