package com.seventh.tickets.web.vo;

import com.seventh.tickets.base.BaseVO;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * ConcertTicketVO
 * <p>
 * Created by yoka24443 on 2018/12/6.
 */
public class ConcertTicketVO implements BaseVO, Serializable {

    private static final long serialVersionUID = -1324728468263204945L;

    /**
     * 席位级别
     */
    @NotNull
    private String grade;
    /**
     * 席位价格
     */
    @NotNull
    private Double price;
    /**
     * 席位总数量
     */
    @NotNull
    private Integer seatTotal;

    public String getGrade() {
        return grade;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Integer getSeatTotal() {
        return seatTotal;
    }

    public void setSeatTotal(Integer seatTotal) {
        this.seatTotal = seatTotal;
    }
}
