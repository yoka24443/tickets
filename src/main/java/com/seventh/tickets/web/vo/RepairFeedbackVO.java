package com.seventh.tickets.web.vo;

import com.seventh.tickets.base.BaseVO;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

/**
 * RepairFeedbackVO
 * <p>
 * Created by yoka24443 on 2019/11/21.
 */
public class RepairFeedbackVO implements BaseVO, Serializable {
    private static final long serialVersionUID = 3163878755492463350L;

    /**
     * 报修Id
     */
    @NotNull
    private Long repairId;

    /**
     * 供应商用户Id
     */
    @NotNull
    private Long supplierId;

    /**
     * 反馈日期
     */
    @NotNull
    private Date feedbackDate;

    /**
     * 反馈解答
     */
    @NotNull
    private String feedbackAnswer;

    /**
     * 供应商快递单号
     */
    private String expressNO;

    public Long getRepairId() {
        return repairId;
    }

    public void setRepairId(Long repairId) {
        this.repairId = repairId;
    }

    public Long getSupplierId() {
        return supplierId;
    }

    public void setSupplierId(Long supplierId) {
        this.supplierId = supplierId;
    }

    public Date getFeedbackDate() {
        return feedbackDate;
    }

    public void setFeedbackDate(Date feedbackDate) {
        this.feedbackDate = feedbackDate;
    }

    public String getFeedbackAnswer() {
        return feedbackAnswer;
    }

    public void setFeedbackAnswer(String feedbackAnswer) {
        this.feedbackAnswer = feedbackAnswer;
    }

    public String getExpressNO() {
        return expressNO;
    }

    public void setExpressNO(String expressNO) {
        this.expressNO = expressNO;
    }
}
