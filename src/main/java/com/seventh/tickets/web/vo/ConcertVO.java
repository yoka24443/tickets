package com.seventh.tickets.web.vo;

import com.seventh.tickets.base.BaseVO;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * ConcertVO
 * <p>
 * Created by yoka24443 on 2018/12/6.
 */
public class ConcertVO implements BaseVO, Serializable {

    private static final long serialVersionUID = 5836338603505182237L;

    /**
     * 演唱会名称
     */
    @NotNull
    private String concertName;
    /**
     * 歌手
     */
    @NotNull
    private String singer;
    /**
     * 开演日期
     */
    @NotNull
    private Date concertDate;
    /**
     * 开演时间
     */
    @NotNull
    private Date concertStartTime;
    /**
     * 演出地点
     */
    @NotNull
    private String concertPlace;
    /**
     * 演唱会的席位集合
     */
    @NotNull.List(value = {})
    private List<ConcertTicketVO> ticketList;

    public String getConcertName() {
        return concertName;
    }

    public void setConcertName(String concertName) {
        this.concertName = concertName;
    }

    public String getSinger() {
        return singer;
    }

    public void setSinger(String singer) {
        this.singer = singer;
    }

    public Date getConcertDate() {
        return concertDate;
    }

    public void setConcertDate(Date concertDate) {
        this.concertDate = concertDate;
    }

    public Date getConcertStartTime() {
        return concertStartTime;
    }

    public void setConcertStartTime(Date concertStartTime) {
        this.concertStartTime = concertStartTime;
    }

    public String getConcertPlace() {
        return concertPlace;
    }

    public void setConcertPlace(String concertPlace) {
        this.concertPlace = concertPlace;
    }

    public List<ConcertTicketVO> getTicketList() {
        return ticketList;
    }

    public void setTicketList(List<ConcertTicketVO> ticketList) {
        this.ticketList = ticketList;
    }
}
