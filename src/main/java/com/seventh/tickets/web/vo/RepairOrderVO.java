package com.seventh.tickets.web.vo;

import com.seventh.tickets.base.BaseVO;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

/**
 * RepairOrderVO
 * <p>
 * Created by yoka24443 on 2019/11/21.
 */
public class RepairOrderVO implements BaseVO, Serializable {

    private static final long serialVersionUID = -4439668348939850607L;

    /**
     * 客户Id
     */
    @NotNull
    private Long customerId;

    /**
     * 设备编号
     */
    @NotNull
    private String deviceNO;

    /**
     * 问题描述
     */
    @NotNull
    private String problem;

    /**
     * 报修日期
     */
    @NotNull
    private Date repairDate;

    /**
     * 开始时间
     */
    private Date startDate;

    /**
     * 结束时间
     */
    private Date endDate;

    /**
     * 客户快递番号
     */
    private String expressNO;

    /**
     * 报修状态
     */
    private Integer status;

    public Long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Long customerId) {
        this.customerId = customerId;
    }

    public String getDeviceNO() {
        return deviceNO;
    }

    public void setDeviceNO(String deviceNO) {
        this.deviceNO = deviceNO;
    }

    public String getProblem() {
        return problem;
    }

    public void setProblem(String problem) {
        this.problem = problem;
    }

    public Date getRepairDate() {
        return repairDate;
    }

    public void setRepairDate(Date repairDate) {
        this.repairDate = repairDate;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getExpressNO() {
        return expressNO;
    }

    public void setExpressNO(String expressNO) {
        this.expressNO = expressNO;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
}
