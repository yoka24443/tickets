package com.seventh.tickets.web.controller;

import com.seventh.tickets.base.Result;
import com.seventh.tickets.entity.Booking;
import com.seventh.tickets.service.BookingService;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
public class BookingController {

    private final BookingService bookingService;

    public BookingController(BookingService bookingService) {
        this.bookingService = bookingService;
    }

    @ApiOperation(value = "用户购票")
    @PostMapping("/addBooking")
    public ResponseEntity<Result> bookingInsert(@RequestParam Long concertId,
                                                @RequestParam Long userId,
                                                @RequestParam String userIdCard,
                                                @RequestParam String seatType) throws Exception {
        Result result;

        if (concertId == null || userId == null || userIdCard == null || seatType == null) {
            throw new Exception("数据不完整，购票失败");
        }

        try {
            result = bookingService.insertBooking(concertId, userId, userIdCard, seatType);

            if (result.hasError()) {
                return ResponseEntity.ok(result);
            }

            return ResponseEntity.ok().body(result);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @ApiOperation(value = "根据身份证号查询演唱会购票信息")
    @GetMapping("/idCardBooking")
    public ResponseEntity<List<Booking>> findUserIdCard(@RequestParam String userIdCard) throws Exception {
        if (userIdCard != null) {
            List<Booking> bookings = bookingService.findByUserIdCard(userIdCard);
            return ResponseEntity.ok().body(bookings);
        } else {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @ApiOperation(value = "查询用户购票信息")
    @GetMapping("/userIdBooking")
    public ResponseEntity<List<Booking>> findUserId(@RequestParam Long userId) throws Exception {
        if (userId != null) {
            List<Booking> bookings = bookingService.findByUserId(userId);
            return ResponseEntity.ok().body(bookings);
        } else {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @ApiOperation(value = "用户退票")
    @PostMapping("/refundBooking")
    public ResponseEntity<Result> refundBooking(@RequestParam String bookingId, @RequestParam String concertId, @RequestParam String userId) throws Exception {
        Result result;

        if (bookingId.isEmpty() || concertId.isEmpty() || userId.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        try {
            result = bookingService.refundBooking(Long.parseLong(bookingId), Long.parseLong(concertId), Long.parseLong(userId));

            if (result.hasError()) {
                return ResponseEntity.ok(result);
            }

            return ResponseEntity.ok().body(result);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }
}
