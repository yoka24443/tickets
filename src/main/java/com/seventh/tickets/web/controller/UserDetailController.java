package com.seventh.tickets.web.controller;

import com.seventh.tickets.entity.UserDetail;
import com.seventh.tickets.service.UserDetailService;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api")
public class UserDetailController {

    private final UserDetailService userDetailService;

    public UserDetailController(UserDetailService userDetailService) {

        this.userDetailService = userDetailService;
    }

    @ApiOperation(value = "获取用户信息")
    @GetMapping("/getUserInfo")
    public ResponseEntity<UserDetail> getUserInfo(@RequestParam String loginName) throws Exception {
        if (loginName != null) {
            UserDetail userDetail = userDetailService.getUserDetail(loginName);
            return ResponseEntity.ok().body(userDetail);
        } else {
            return new ResponseEntity<UserDetail>(HttpStatus.BAD_REQUEST);
        }
    }

    @ApiOperation(value = "修改用户信息")
    @PostMapping("/updateUserInfo")
    public ResponseEntity<UserDetail> updateUserInfo(@RequestParam String userLoginName,
                                                     @RequestParam String userName,
                                                     @RequestParam String userPhone,
                                                     @RequestParam String userLivePlace,
                                                     @RequestParam String userEmail,
                                                     @RequestParam String userIdCard) throws Exception {
        UserDetail userDetail = userDetailService.updateUserDetail(userLoginName, userName, userPhone, userLivePlace, userEmail, userIdCard);
        if (userDetail != null) {
            return ResponseEntity.ok().body(userDetail);
        } else {
            return new ResponseEntity<UserDetail>(HttpStatus.BAD_REQUEST);
        }
    }
}
