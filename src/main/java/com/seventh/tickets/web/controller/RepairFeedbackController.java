package com.seventh.tickets.web.controller;

import com.seventh.tickets.base.Result;
import com.seventh.tickets.service.RepairFeedbackService;
import com.seventh.tickets.web.util.DateUtil;
import com.seventh.tickets.web.vo.RepairFeedbackVO;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * RepairFeedbackController
 * <p>
 * Created by yoka24443 on 2019/11/21.
 */
@RestController
@RequestMapping("/api")
public class RepairFeedbackController {

    private final RepairFeedbackService repairFeedbackService;

    public RepairFeedbackController(RepairFeedbackService repairFeedbackService) {
        this.repairFeedbackService = repairFeedbackService;
    }

    @ApiOperation(value = "报修反馈")
    @PostMapping("/addRepairFeedback")
    public ResponseEntity<Result> addRepairFeedback(@RequestParam Long repairId,
                                                    @RequestParam Long supplierId,
                                                    @RequestParam String feedbackDate,
                                                    @RequestParam String feedbackAnswer,
                                                    @RequestParam String expressNO) throws Exception {
        Result result;

        RepairFeedbackVO vo = new RepairFeedbackVO();
        vo.setRepairId(repairId);
        vo.setSupplierId(supplierId);
        vo.setFeedbackDate(DateUtil.getDate(feedbackDate, DateUtil.REG_YYMMDD));
        vo.setFeedbackAnswer(feedbackAnswer);
        vo.setExpressNO(expressNO);

        try {
            result = repairFeedbackService.addRepairFeedback(vo);

            if (result.hasError()) {
                return ResponseEntity.ok(result);
            }

            return ResponseEntity.ok().body(result);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @ApiOperation(value = "获取报修单反馈")
    @PostMapping("/getRepairFeedbackByRepairId")
    public ResponseEntity<Result> addRepairFeedback(@RequestParam Long repairId,
                                                    @RequestParam Long repairFeedbackId) throws Exception {
        Result result;

        try {
            result = repairFeedbackService.getRepairFeedback(repairId, repairFeedbackId);

            if (result.hasError()) {
                return ResponseEntity.ok(result);
            }

            return ResponseEntity.ok().body(result);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }
}
