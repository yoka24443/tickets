package com.seventh.tickets.web.controller;

import com.seventh.tickets.base.Result;
import com.seventh.tickets.entity.User;
import com.seventh.tickets.entity.UserDetail;
import com.seventh.tickets.service.UserDetailService;
import com.seventh.tickets.service.UserService;
import com.seventh.tickets.web.vo.UserVO;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by yoka24443 on 2018/12/5.
 */
@RestController
@RequestMapping("api/user/")
public class UserManagerController {
    @Autowired
    private UserService userService;
    @Autowired
    private UserDetailService userDetailService;

    @ApiOperation(value = "用户管理")
    @GetMapping("/list")
    public ResponseEntity<List<User>> listUser(String keywords) throws Exception {
        List<User> userList = userService.listUser(keywords);
        if (userList != null && !userList.isEmpty()) {
            return ResponseEntity.ok().body(userList);
        } else {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @ApiOperation(value = "获取用户信息")
    @GetMapping("/get")
    public ResponseEntity<UserDetail> getUser(@RequestParam Long userId) throws Exception {
        if (userId != null) {
            UserDetail userDetail = userDetailService.getUserById(userId);
            return ResponseEntity.ok().body(userDetail);
        } else {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @ApiOperation(value = "用户管理")
    @PostMapping("/update")
    public ResponseEntity<UserDetail> updateUser(UserVO vo) throws Exception {
        UserDetail userDetail = userDetailService.getUserById(vo.getUserId());
        User user = userDetail.getUser();

        // 更新用户基本信息
        user.setUserType(vo.getUserType());
        user.setUserStatus(vo.getUserStatus());
        // 更新用户详细信息
        userDetail.setUserName(vo.getUserName());
        userDetail.setUserGender(vo.getGender());
        userDetail.setUserIdCard(vo.getIdCard());
        userDetail.setUserEmail(vo.getEmail());
        userDetail.setUserLivePlace(vo.getLivePlace());

        User user1 = userService.updateUser(user);
        UserDetail userDetail1 = userDetailService.updateUserDetail(userDetail);

        if (user1 != null && userDetail1 != null) {
            return ResponseEntity.ok().body(userDetail1);
        } else {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @ApiOperation(value = "修改密码")
    @PostMapping("/delete")
    public ResponseEntity<Result> changePassword(@RequestParam Long userId) throws Exception {
        Result result;

        try {
            result = userService.deleteUser(userId);
            if (result.hasError()) {
                return ResponseEntity.ok(result);
            }
            return ResponseEntity.ok().body(result);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }
}
