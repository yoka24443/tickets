package com.seventh.tickets.web.controller;

import com.seventh.tickets.service.PickUpAcceptService;
import com.seventh.tickets.service.PickUpApplyService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * PickUpAcceptController
 *
 */
@RestController
@RequestMapping("/api/pickup/accept")
public class PickUpAcceptController {
    private final PickUpAcceptService pickUpAcceptService;

    public PickUpAcceptController(PickUpAcceptService pickUpAcceptService) {
        this.pickUpAcceptService = pickUpAcceptService;
    }
}
