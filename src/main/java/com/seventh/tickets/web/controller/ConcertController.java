package com.seventh.tickets.web.controller;

import com.seventh.tickets.entity.Concert;
import com.seventh.tickets.service.ConcertService;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
public class ConcertController {

    private final ConcertService concertService;

    public ConcertController(ConcertService concertService) {
        this.concertService = concertService;
    }


    @ApiOperation(value = "新增演唱会信息")
    @PostMapping("/addConcert")
    public ResponseEntity<Concert> concertInsert(@RequestParam String concertName,
                                                 @RequestParam String singer,
                                                 @RequestParam String concertDate,
                                                 @RequestParam String concertStartTime,
                                                 @RequestParam String concertPlace,
                                                 @RequestParam String grade_1,
                                                 @RequestParam String price_1,
                                                 @RequestParam String seat_1,
                                                 @RequestParam String grade_2,
                                                 @RequestParam String price_2,
                                                 @RequestParam String seat_2,
                                                 @RequestParam String grade_3,
                                                 @RequestParam String price_3,
                                                 @RequestParam String seat_3
    ) throws Exception {

        Concert concert = concertService.insertConcert(concertName, singer, concertDate, concertStartTime, concertPlace, grade_1, price_1, seat_1, grade_2, price_2, seat_2, grade_3, price_3, seat_3);
        if (concert != null) {
            return ResponseEntity.ok().body(concert);
        } else {
            return new ResponseEntity<Concert>(HttpStatus.BAD_REQUEST);
        }
    }


    @ApiOperation(value = "修改演唱会信息")
    @PutMapping("/modifyDateConcert")
    public ResponseEntity<Concert> updateConcert(@RequestParam Long concertId,
                                                 @RequestParam String concertName,
                                                 @RequestParam String singer,
                                                 @RequestParam String concertDate,
                                                 @RequestParam String concertStartTime,
                                                 @RequestParam String concertPlace) throws Exception {

        Concert concert = concertService.updateConcert(concertId, concertName, singer, concertDate, concertStartTime, concertPlace);
        if (concert != null) {
            return ResponseEntity.ok().body(concert);
        } else {
            return new ResponseEntity<Concert>(HttpStatus.BAD_REQUEST);
        }
    }


    @ApiOperation(value = "删除演唱会信息")
    @PostMapping("/deleteConcert")
    public ResponseEntity<String> deleteConcert(@RequestParam String concert_id) throws Exception {

        System.out.println(concert_id);
        if (concert_id != null) {
            String status = concertService.deleteConcert(Long.parseLong(concert_id));
            return ResponseEntity.ok().body(status);
        } else {
            return new ResponseEntity<String>(HttpStatus.BAD_REQUEST);
        }
    }

    @ApiOperation(value = "关键字查询演唱会信息")
    @GetMapping("/keywordsConcert")
    public ResponseEntity<List<Concert>> findKeywordsConcert(@RequestParam String keywords) throws Exception {

        if (keywords != null) {
            List<Concert> concerts = concertService.findByKeywords(keywords);
            return ResponseEntity.ok().body(concerts);
        } else {
            return new ResponseEntity<List<Concert>>(HttpStatus.BAD_REQUEST);
        }
    }

    @ApiOperation(value = "查询所有演唱会信息")
    @GetMapping("/allConcert")
    public ResponseEntity<List<Concert>> findAllConcert() throws Exception {
        List<Concert> concerts = concertService.findAllConcerts();
        if (concerts.size() > 0) {
            return ResponseEntity.ok().body(concerts);
        } else {
            return new ResponseEntity<List<Concert>>(HttpStatus.BAD_REQUEST);
        }
    }

    @ApiOperation(value = "根据演唱会id查询信息")
    @GetMapping("/idConcert")
    public ResponseEntity<Concert> findById(@RequestParam Long concertId) throws Exception {
        Concert concert = concertService.findByConcertId(concertId);
        if (concert != null) {
            return ResponseEntity.ok().body(concert);
        } else {
            return new ResponseEntity<Concert>(HttpStatus.BAD_REQUEST);
        }
    }


}
