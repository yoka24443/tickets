package com.seventh.tickets.web.controller;

import com.seventh.tickets.entity.ConcertTicket;
import com.seventh.tickets.service.ConcertTicketService;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api")
public class ConcertTicketController {

    private final ConcertTicketService concertTicketService;

    public ConcertTicketController(ConcertTicketService concertTicketService) {

        this.concertTicketService = concertTicketService;
    }

    @ApiOperation(value = "根据演唱会id查询所有票务信息")
    @GetMapping("/idConcertTicket")
    public ResponseEntity<List<ConcertTicket>> findByIdConcertTickets(@RequestParam Long concertId) throws Exception {
        List<ConcertTicket> concertTickets = concertTicketService.findByConcertId(concertId);
        if (concertTickets.size() != 0) {
            return ResponseEntity.ok().body(concertTickets);
        } else {
            return new ResponseEntity<List<ConcertTicket>>(HttpStatus.BAD_REQUEST);
        }
    }

}
