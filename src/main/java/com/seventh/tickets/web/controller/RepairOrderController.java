package com.seventh.tickets.web.controller;

import com.seventh.tickets.base.Result;
import com.seventh.tickets.service.RepairOrderService;
import com.seventh.tickets.web.util.DateUtil;
import com.seventh.tickets.web.vo.RepairOrderVO;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;

/**
 * RepairOrderController
 * <p>
 * Created by yoka24443 on 2019/11/21.
 */
@RestController
@RequestMapping("/api")
public class RepairOrderController {
    private final RepairOrderService repairOrderService;

    public RepairOrderController(RepairOrderService repairOrderService) {
        this.repairOrderService = repairOrderService;
    }

    @ApiOperation(value = "用户报修")
    @PostMapping("/addRepairOrder")
    public ResponseEntity<Result> addRepairOrder(@RequestParam Long customerId,
                                                 @RequestParam String deviceNO,
                                                 @RequestParam String problem,
                                                 @RequestParam String repairDate,
                                                 @RequestParam String expressNO) throws Exception {

        Result result;

        RepairOrderVO vo = new RepairOrderVO();
        vo.setCustomerId(customerId);
        vo.setDeviceNO(deviceNO);
        vo.setProblem(problem);
        vo.setRepairDate(DateUtil.getDate(repairDate, DateUtil.REG_YYMMDD));
        vo.setExpressNO(expressNO);

        try {
            result = repairOrderService.addRepairOrder(vo);

            if (result.hasError()) {
                return ResponseEntity.ok(result);
            }

            return ResponseEntity.ok().body(result);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @ApiOperation(value = "查询用户报修")
    @GetMapping("/findRepairOrder")
    public ResponseEntity<Result> findRepairOrder(@RequestParam Long customerId,
                                                  @RequestParam(required = false) String deviceNO,
                                                  @RequestParam(required = false) String startDate,
                                                  @RequestParam(required = false) String endDate,
                                                  @RequestParam(required = false) String expressNO,
                                                  @RequestParam(required = false) Integer status) throws Exception {
        Result result;

        RepairOrderVO vo = new RepairOrderVO();

        vo.setCustomerId(customerId);

        // 设备号
        if (deviceNO != null && deviceNO.length() > 0) {
            vo.setDeviceNO(deviceNO);
        }

        // 报修开始日期
        if (startDate != null && startDate.length() > 0) {
            Date dStartDate = DateUtil.getDate(startDate);
            if (dStartDate != null) {
                vo.setStartDate(dStartDate);
            }
        }

        // 报修开始日期
        if (endDate != null && endDate.length() > 0) {
            Date dEndDate = DateUtil.getDate(endDate);
            if (dEndDate != null) {
                vo.setEndDate(dEndDate);
            }
        }

        // 快递单号
        if (expressNO != null && expressNO.length() > 0) {
            vo.setExpressNO(expressNO);
        }

        // 报修状态
        if (status != null) {
            vo.setStatus(status);
        }

        try {
            result = repairOrderService.findRepairOrder(vo);
            if (result.hasError()) {
                return ResponseEntity.ok(result);
            }

            return ResponseEntity.ok().body(result);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @ApiOperation(value = "查询用户报修")
    @GetMapping("/findAllRepairOrder")
    public ResponseEntity<Result> findAllRepairOrder(@RequestParam(required = false) String deviceNO,
                                                     @RequestParam(required = false) String startDate,
                                                     @RequestParam(required = false) String endDate,
                                                     @RequestParam(required = false) String expressNO,
                                                     @RequestParam(required = false) Integer status) throws Exception {
        Result result;

        RepairOrderVO vo = new RepairOrderVO();

        // 设备号
        if (deviceNO != null && deviceNO.length() > 0) {
            vo.setDeviceNO(deviceNO);
        }

        // 报修开始日期
        if (startDate != null && startDate.length() > 0) {
            Date dStartDate = DateUtil.getDate(startDate, DateUtil.REG_YYMMDD);
            if (dStartDate != null) {
                vo.setStartDate(dStartDate);
            }
        }

        // 报修开始日期
        if (endDate != null && endDate.length() > 0) {
            Date dEndDate = DateUtil.getDate(endDate, DateUtil.REG_YYMMDD);
            if (dEndDate != null) {
                vo.setEndDate(dEndDate);
            }
        }

        // 快递单号
        if (expressNO != null && expressNO.length() > 0) {
            vo.setExpressNO(expressNO);
        }

        // 报修状态
        if (status != null) {
            vo.setStatus(status);
        }

        try {
            result = repairOrderService.findAllRepairOrder(vo);
            if (result.hasError()) {
                return ResponseEntity.ok(result);
            }

            return ResponseEntity.ok().body(result);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @ApiOperation(value = "获取报修单详情")
    @GetMapping("/getRepairOrderDetail")
    public ResponseEntity<Result> getRepairOrderDetail(@RequestParam Long repairId) throws Exception {
        Result result;

        try {
            result = repairOrderService.getRepairOrderDetail(repairId);
            if(result.hasError()) {
                return ResponseEntity.ok(result);
            }

            return ResponseEntity.ok().body(result);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }
}
