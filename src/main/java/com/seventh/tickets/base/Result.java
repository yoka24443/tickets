package com.seventh.tickets.base;

/**
 * Result
 *
 * Created by yoka24443 on 2018/12/5.
 */
public interface Result {

    void setError(Boolean b);

    Boolean hasError();

    void setErrorCode(BaseCode error);

    Object getData();

    void setData(Object obj);

    BaseCode getErrorCode();

    String getErrorMessage();
}
