package com.seventh.tickets.base;

/**
 * BaseCode
 *
 * Created by yoka24443 on 2018/12/4.
 */
public interface BaseCode<T> {
    String getCode();

    String getText();
}
