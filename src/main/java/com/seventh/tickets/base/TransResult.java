package com.seventh.tickets.base;

import java.io.Serializable;

/**
 * TransResult
 * <p>
 * Created by yoka24443 on 2018/12/5.
 */
public class TransResult implements Result, Serializable {

    private static final long serialVersionUID = 3723370784406133362L;
    /**
     * 是否出错
     */
    private boolean status = false;
    /**
     * 错误码
     */
    private BaseCode errorCode;
    /**
     * 返回的结果
     */
    private Object data;
    /**
     * 错误消息
     */
    private String errorMessage;

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    @Override
    public void setError(Boolean isError) {
        status = isError;
        if (errorCode != null) {
            status = true;
        }
    }

    @Override
    public Boolean hasError() {
        return status;
    }

    @Override
    public void setErrorCode(BaseCode errorCode) {
        if (errorCode != null) {
            status = true;
        }
        this.errorCode = errorCode;
        this.errorMessage = errorCode.getText();
    }

    @Override
    public Object getData() {
        return this.data;
    }

    @Override
    public void setData(Object obj) {
        this.data = obj;
    }

    @Override
    public BaseCode getErrorCode() {
        return this.errorCode;
    }

    @Override
    public String getErrorMessage() {
        errorMessage = this.errorCode != null ? this.errorCode.getText() : "";
        return errorMessage;
    }
}
