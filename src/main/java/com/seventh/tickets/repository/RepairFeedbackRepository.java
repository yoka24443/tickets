package com.seventh.tickets.repository;

import com.seventh.tickets.entity.RepairFeedback;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * RepairFeedbackRepository
 *
 * Created by yoka24443 on 2019/11/21.
 */
@Repository
public interface RepairFeedbackRepository extends JpaRepository<RepairFeedback, Long> {
    /**
     * 获取报修单反馈
     *
     * @param repairId
     * @param repairFeedbackId
     *
     * @return
     */
    @Query("select a from RepairFeedback a where a.repairOrder.repairId = ?1 and  a.repairFeedbackId = ?2")
    RepairFeedback getRepairFeedback(Long repairId, Long repairFeedbackId);
}
