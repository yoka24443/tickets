package com.seventh.tickets.repository;

import com.seventh.tickets.entity.UserDetail;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@SuppressWarnings("unused")
@Repository
public interface UserDetailRepository extends JpaRepository<UserDetail, Long> {

    /**
     * 查询用户详细信息(ByLoginName)
     *
     * @param userLoginName
     * @return
     */
    @Query("select a from UserDetail a where a.userLoginName = ?1")
    UserDetail findByUserLoginName(String userLoginName);

    /**
     * 查询用户详细信息(ByUserId)
     *
     * @param userId
     * @return
     */
    @Query("select a from UserDetail a where a.user.userId = ?1")
    UserDetail findByUserId(Long userId);
}
