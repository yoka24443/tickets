package com.seventh.tickets.repository;


import com.seventh.tickets.entity.PickUpAccept;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * PickUpAcceptRepository
 *
 *
 */
@Repository
public interface PickUpAcceptRepository extends JpaRepository<PickUpAccept, Integer> {
}
