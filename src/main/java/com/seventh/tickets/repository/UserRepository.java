package com.seventh.tickets.repository;

import com.seventh.tickets.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@SuppressWarnings("unused")
@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    /**
     * 查询用户信息(ByLoginName)
     *
     * @param userLoginName
     * @return
     */
    @Query("select a from User a where a.userLoginName=?1")
    User findByUserLoginName(String userLoginName);

    /**
     * 查询用户信息(ByUserId)
     *
     * @param userId
     * @return
     */
    @Query("select a from User a where a.userId = ?1")
    User findByUserId(Long userId);
}
