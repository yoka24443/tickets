package com.seventh.tickets.repository;

import com.seventh.tickets.entity.RepairOrder;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

/**
 * RepairOrderRepository
 * <p>
 * Created by yoka24443 on 2019/11/21.
 */
@Repository
public interface RepairOrderRepository extends JpaRepository<RepairOrder, Long> {

    @Query("select a from RepairOrder a where 1=1" +
            " and ((?1 is null or a.deviceNO like concat('%', ?1, '%'))" +
            " and (?2 is null or a.problem like concat('%', ?2, '%'))" +
            " and (?3 is null or a.customerExpressNO like concat('%', ?3, '%')))" +
            " and (?4 is null or a.repairDate >= ?4)" +
            " and (?5 is null or a.repairDate <= ?5)" +
            " and (?6 is null or a.repairStatus = ?6)")
    List<RepairOrder> findAllByVO(String deviceNO, String problem, String expressNO, Date startDate, Date endDate, Integer status);
}
