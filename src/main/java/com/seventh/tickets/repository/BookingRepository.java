package com.seventh.tickets.repository;

import com.seventh.tickets.entity.Booking;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@SuppressWarnings("unused")
@Repository
public interface BookingRepository extends JpaRepository<Booking, Long> {

    @Query("select a from Booking a where a.userIdCard=?1")
    List<Booking> findAllByIdCard(String userIdCard);

    @Query("select a from Booking a where a.userId=?1")
    List<Booking> findAllByUserId(Long userId);
}
