package com.seventh.tickets.repository;

import com.seventh.tickets.entity.ConcertTicket;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@SuppressWarnings("unused")
@Repository
public interface ConcertTicketRepository extends JpaRepository<ConcertTicket, Long> {

    @Query("select a from ConcertTicket a where a.concert.concertId=?1")
    List<ConcertTicket> findAllByConcertId(Long concertId);

    @Query("select a from ConcertTicket a where a.concert.concertId=?1 and a.seatGrade=?2")
    ConcertTicket findByConcertIdAndSeatGrade(Long concertId, String seatGrade);
}
