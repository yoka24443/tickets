package com.seventh.tickets.repository;

import com.seventh.tickets.entity.Concert;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@SuppressWarnings("unused")
@Repository
public interface ConcertRepository extends JpaRepository<Concert, Long> {

    @Query("select a from Concert a where a.concertName like concat('%', ?1, '%') and a.concertDate = ?2")
    Concert findByConcertNameAndConcertDate(String concertName, Date concertDate);

    @Query("select a from Concert a where" +
            " (a.concertName like concat('%', ?1, '%') " +
            "or a.concertPlace like concat('%', ?1, '%') " +
//            "or (a.concertDate like concat('%', ?1, '%')) " +
//            "or a.concertStartTime like concat('%', ?1, '%') " +
            "or a.singer like concat('%', ?1, '%') )")
    List<Concert> findByKeywords(String keywords);
}
