package com.seventh.tickets.repository;

import com.seventh.tickets.entity.PickUpApply;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * PickUpApplyRepository
 *
 */
@Repository
public interface PickUpApplyRepository extends JpaRepository<PickUpApply, Integer> {

}
