package com.seventh.tickets.enums;

import com.seventh.tickets.base.BaseCode;

/**
 * UserErrorCode
 * <p>
 * Created by yoka24443 on 2018/12/5.
 */
public enum UserErrorCode implements BaseCode {
    NOT_EXISTS("1099", "用户名或密码错误"),
    ORIGIN_PASSWORD_INVALID("1090", "原密码无效"),
    LOGIN_SUCCESS("1010", "登录成功"),
    LOGIN_FAILED("1020", "登录失败");

    UserErrorCode(String code, String message) {
        this.code = code;
        this.message = message;
    }

    private String code;
    private String message;

    @Override
    public String getCode() {
        return code;
    }

    @Override
    public String getText() {
        return message;
    }
}
