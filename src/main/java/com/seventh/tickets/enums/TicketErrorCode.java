package com.seventh.tickets.enums;

import com.seventh.tickets.base.BaseCode;

/**
 * ConcertErrorCode
 * <p>
 * Created by yoka24443 on 2018/12/6.
 */
public enum TicketErrorCode implements BaseCode {
    TICKET_ADD_EXCEPTION("3099", "添加席位票数据出现异常"),
    TICKET_IS_NOT_EXISTS("3090", "未查询到该演唱会的席位信息"),
    TICKET_IS_SOLD_OUT("3010", "席位已售罄"),
    ;

    private String code;
    private String message;

    TicketErrorCode(String code, String message) {
        this.code = code;
        this.message = message;
    }

    @Override
    public String getCode() {
        return null;
    }

    @Override
    public String getText() {
        return null;
    }
}
