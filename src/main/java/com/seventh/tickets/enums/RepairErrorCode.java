package com.seventh.tickets.enums;

import com.seventh.tickets.base.BaseCode;

/**
 * Created by yoka24443 on 2019/11/21.
 */
public enum RepairErrorCode implements BaseCode {
    REPAIR_ORDER_IS_NOT_EXISTS("7090", "未查到该报修单信息"),
    REPAIR_ORDER_IS_REPAIRED("7092", "该报修单已经完成报修反馈"),
    ;

    private String code;
    private String message;

    RepairErrorCode(String code, String message) {
        this.code = code;
        this.message = message;
    }

    @Override
    public String getCode() {
        return null;
    }

    @Override
    public String getText() {
        return null;
    }
}
