package com.seventh.tickets.enums;

import com.seventh.tickets.base.BaseCode;

/**
 * RepairStatus
 *
 * Created by yoka24443 on 2019/11/21.
 */
public enum RepairStatus implements BaseCode {
    IS_REPAIRED(0, "已报修"),
    IS_FEEDBACK(1, "已反馈"),
    ;

    private Integer code;
    private String desc;

    RepairStatus(Integer code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public Integer getStatus() {
        return this.code;
    }

    @Override
    public String getCode() {
        return this.code.toString();
    }

    @Override
    public String getText() {
        return this.desc;
    }
}
