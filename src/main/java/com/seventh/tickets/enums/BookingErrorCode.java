package com.seventh.tickets.enums;

import com.seventh.tickets.base.BaseCode;

/**
 * BookingErrorCode
 * <p>
 * Created by yoka24443 on 2018/12/5.
 */
public enum BookingErrorCode implements BaseCode {
    BOOKING_IS_NOT_EXISTS("4090", "该席位票不存在"),
    BOOKING_REFUND_CONCERT_IS_STARTED("4091", "演出已开始不能退票"),;

    private String code;
    private String text;

    BookingErrorCode(String code, String text) {
        this.code = code;
        this.text = text;
    }

    @Override
    public String getCode() {
        return null;
    }

    @Override
    public String getText() {
        return null;
    }
}
