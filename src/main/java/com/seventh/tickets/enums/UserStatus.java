package com.seventh.tickets.enums;

import com.seventh.tickets.base.BaseCode;

/**
 * UserStatus
 * <p>
 * Created by yoka24443 on 2018/12/4.
 */
public enum UserStatus implements BaseCode {
    IS_NORMAL("0", "正常"),
    IS_LOCKED("1", "停用"),
    IS_DELETED("2", "删除"),;

    private String code;
    private String text;

    UserStatus(String code, String message) {
        this.code = code;
        this.text = message;
    }

    @Override
    public String getCode() {
        return this.code;
    }

    @Override
    public String getText() {
        return this.text;
    }
}
