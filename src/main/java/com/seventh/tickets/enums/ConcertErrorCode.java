package com.seventh.tickets.enums;

import com.seventh.tickets.base.BaseCode;

/**
 * ConcertErrorCode
 * <p>
 * Created by yoka24443 on 2018/12/6.
 */
public enum ConcertErrorCode implements BaseCode {
    CONCERT_ADD_EXCEPTION("2099", "添加演唱会数据出现异常"),
    CONCERT_IS_NOT_EXISTS("2090", "未查到该演唱会信息"),
    CONCERT_IS_STARTED("2010", "演出已开始"),
    CONCERT_IS_EXPIRED("2020", "演出已过期"),
    ;

    private String code;
    private String message;

    ConcertErrorCode(String code, String message) {
        this.code = code;
        this.message = message;
    }

    @Override
    public String getCode() {
        return null;
    }

    @Override
    public String getText() {
        return null;
    }
}
